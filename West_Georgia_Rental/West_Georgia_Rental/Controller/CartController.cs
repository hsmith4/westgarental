﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using West_Georgia_Rental.DAL;
using West_Georgia_Rental.Model;

namespace West_Georgia_Rental.Controller
{
   
    class CartController
    {
        //Calls DAL to add new item
        public int InsertCart(int memberID, int serialNumber, int empID, int length)
        {
            return CartDAL.addToCart(memberID, serialNumber, empID, length);
        }

        //Calls DAL to get cart by member
        public List<Cart> GetCartByMemberID(int memberID)
        {
            return CartDAL.getCartByMemberID(memberID);
        }

        //Calls DAL to count all items in cart
        public int GetCountOfMemberCart(int memberID)
        {
            return CartDAL.countItemsInMemberCart(memberID);
        }

        //Calls DAL to delete selected item
        public int DeleteCartItem(int memberID, int serialNumber)
        {
            return CartDAL.deleteCartItem(memberID, serialNumber);
        }

        //Calls DAL to get cart total
        public decimal GetCartTotalForMember(int memberID)
        {
            return CartDAL.getCartTotalForMember(memberID);
        }

        //Calls DAL to get item total
        public decimal GetItemTotal(int serialNumber)
        {
            return CartDAL.getItemTotal(serialNumber);
        }

        //Calls DAL to get item by serial number
        public Cart GetCartItemBySerialNumber(int serialNumber)
        {
            return CartDAL.getCartItemBySerialNumber(serialNumber);
        }

        //Calls DAL to update the selected item
        public int UpdateCartItem(int length, int serialNumber)
        {
            return CartDAL.updateCartItem(serialNumber, length);
        }

        //Calls DAL to to return a list of all carts
        public List<Cart> getAllCarts()
        {
            return CartDAL.getAllCarts();
        }

        //Returns false if item is not in cart
        public bool isItemInCart(int serial)
        {
            return CartDAL.isItemInCart(serial);
        }

        //Calls DAL to get return date
        public DateTime getItemReturnDate(int memberID, int serialNumber)
        {
            return CartDAL.getItemReturnDate(memberID, serialNumber);
        }
    }
}
