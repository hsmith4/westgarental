﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using West_Georgia_Rental.DAL;
using West_Georgia_Rental.Model;

namespace West_Georgia_Rental.Controller
{
    public class ReturnController
    {
        //Calls DAL to add a return listing with employee
        public int returnLineItem(int empID)
        {
            return ReturnDAL.returnLineItem(empID);
        }

        //Calls DAL to get all returns by member id
        public List<Return> getReturnsByMember(int memberID)
        {
            return ReturnDAL.getReturnsByMember(memberID);
        }
    }
}
