﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using West_Georgia_Rental.DAL;
using West_Georgia_Rental.Model;

namespace West_Georgia_Rental.Controller
{
    public class StyleController
    {
        //Calls DAL to get all styles
        public List<FurnitureListing> getAllStyles()
        {
            return StyleDAL.getAllStyles();
        }
    }
}
