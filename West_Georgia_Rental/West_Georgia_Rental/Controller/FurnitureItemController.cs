﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using West_Georgia_Rental.DAL;
using West_Georgia_Rental.Model;

namespace West_Georgia_Rental.Controller
{
    public class FurnitureItemController
    {
        //Calls DAL to get minimum serial
        public int getMinSerial()
        {
            return FurnitureItemDAL.getMinSerial();
        }

        //Calls DAL to get max serial 
        public int getMaxSerial()
        {
            return FurnitureItemDAL.getMaxSerial();
        }

        //Calls DAL to get item by serial number
        public FurnitureListing getItemBySerialNumber(int serialNumber)
        {
            return FurnitureItemDAL.getItemListing(serialNumber);
        }

        //Calls DAL to check availability
        public int getItemAvailability(int serialNumber)
        {
            return FurnitureItemDAL.checkItemAvailability(serialNumber);
        }

        //Returns true if item is available
        public bool isItemAvailable(int serialNumber)
        {
            return FurnitureItemDAL.isItemAvailable(serialNumber);
        }
    }
}
