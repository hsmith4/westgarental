﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using West_Georgia_Rental.DAL;
using West_Georgia_Rental.Model;

namespace West_Georgia_Rental.Controller
{
    public class CategoryController
    {
        //Calls DAL to get all categories
        public List<FurnitureListing> getAllCategories()
        {
            return CategoryDAL.getAllCategories();
        }
    }
}
