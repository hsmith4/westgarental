﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using West_Georgia_Rental.DAL;
using West_Georgia_Rental.Model;

namespace West_Georgia_Rental.Controller
{
    class MemberController
    {
        //Calls DAL to add a new member
        public int AddMember(DateTime DOB, string fName, string minit, string lName, string phone, string email, string address1, string address2, string city, string state, string zip)
        {
            return MemberDAL.createMember(DOB, fName, minit, lName, phone, email, address1, address2, city, state, zip);
        }

        //Calls DAL to get member by phone or full name
        public List<Member>getMemberByPhoneOrName(string phone, string first, string last)
        {
            return MemberDAL.getMemberByPhoneOrName(phone, first, last);
        }

        //Calls DAL to get all members
        public List<Member>getAllMembers()
        {
            return MemberDAL.getAllMembers();
        }

        //Calls DAL to get member by id
        public Member GetMemberById(int memberID)
        {
            return MemberDAL.getMemberByID(memberID);
        }

        //Returns true if member was updated
        public bool UpdateMember(int memberID, string fName, string minit, string lName, DateTime DOB, string phone, string email, string address1, string address2, string city, string state, string zip)
        {
            return MemberDAL.updateMember(memberID, fName, minit, lName, DOB, phone, email, address1, address2, city, state, zip);
        }

        //Deletes member with selected id
        public int DeleteMember(int memberID)
        {
            return MemberDAL.deleteMember(memberID);
        }
    }
}
