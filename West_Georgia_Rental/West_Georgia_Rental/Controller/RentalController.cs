﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using West_Georgia_Rental.DAL;

namespace West_Georgia_Rental.Controller
{
    class RentalController
    {
        //Calls DAL to create rental by member and employee
        public int CreateRental(int memberID, int empID)
        {
            return RentalDAL.createRental(memberID, empID);
        }

        //Calls DAL to get earliest date
        public DateTime getMinDate()
        {
            return RentalDAL.getMinDate();
        }
    }
}
