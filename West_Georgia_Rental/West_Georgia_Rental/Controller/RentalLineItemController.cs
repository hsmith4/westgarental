﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using West_Georgia_Rental.DAL;
using West_Georgia_Rental.Model;

namespace West_Georgia_Rental.Controller
{
    public class RentalLineItemController
    {
        //Calls DAL to get all rentals by member id
        public List<Rental> getRentalsByID(int memberID)
        {
            return RentalLineItemDAL.getRentalsByID(memberID);
        }

        //Calls DAL to get rental by id and serial number
        public Rental getRentalItemBySerialNumber(int rentalID, int serialNumber)
        {
            return RentalLineItemDAL.getRentalItemBySerialNumber(rentalID, serialNumber);
        }

        //Calls DAL to create new line item
        public int createLineItem(int rentalID, int serialNumber, int memberID)
        {
            return RentalLineItemDAL.createRentalLineItem(rentalID, serialNumber, memberID);
        }

        //Returns true if line item is updated
        public bool updateLineItemRental(int rentalID, int returnID, int serialNumber)
        {
            return RentalLineItemDAL.updateLineItemRental(rentalID, returnID, serialNumber);
        }

        //Calls DAL to return the rental status
        public string getRentalStatus(int rentalID, int serialNumber)
        {
            return RentalLineItemDAL.getRentalStatus(rentalID, serialNumber);
        }
    }
}
