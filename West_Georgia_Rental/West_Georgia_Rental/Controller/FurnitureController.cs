﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using West_Georgia_Rental.DAL;
using West_Georgia_Rental.Model;

namespace West_Georgia_Rental.Controller
{
    public class FurnitureController
    {
        //Calls DAL to get the furniture item with matching serial
        public List<FurnitureListing> theListing(int serialNumber)
        {
            return FurnitureDAL.theListing(serialNumber);
        }

        //Calls DAL to get all listings
        public List<FurnitureListing> allListings()
        {
            return FurnitureDAL.allListings();
        }

        //Calls DAL to get all descriptions
        public List<FurnitureListing> getDescription()
        {
            return FurnitureDAL.getDescription();
        }

        //Calls DAL to get listings by category
        public List<FurnitureListing> theListingsByCategory(string category)
        {
            return FurnitureDAL.theListingsByCategory(category);
        }

        //Calls DAL to get listings by style
        public List<FurnitureListing> theListingsByStyle(string style)
        {
            return FurnitureDAL.theListingsByStyle(style);
        }

        //Calls DAL to get listings by description
        public List<FurnitureListing> theListingsByDescription(string description)
        {
            return FurnitureDAL.theListingsByDescription(description);
        }

        //Calls DAL to get listings by description and style
        public List<FurnitureListing> theListingsByStyleAndDescription(string style, string description)
        {
            return FurnitureDAL.theListingsByStyleAndDescription(style, description);
        }

        //Calls DAL to get listings by description and category
        public List<FurnitureListing> theListingsByCategoryAndDescription(string category, string description)
        {
            return FurnitureDAL.theListingsByCategoryAndDescription(category, description);
        }

        //Calls DAL to get listings by category and style
        public List<FurnitureListing> theListingsByCategoryAndStyle(string category, string style)
        {
            return FurnitureDAL.theListingsByCategoryAndStyle(category, style);
        }

        //Calls DAL to get listings by all attributes
        public List<FurnitureListing> theListingsByAllAttributes(string category, string style, string description)
        {
            return FurnitureDAL.theListingsByAllAttributes(category, style, description);
        }

        //Calls DAL to get minimum id
        public int getMinID()
        {
            return FurnitureDAL.getMinID();
        }

        //Calls DAL to get max id
        public int getMaxID()
        {
            return FurnitureDAL.getMaxID();
        }

        //Calls DAL to get listing by serial
        public FurnitureListing getFurnitureBySerialNumber(int serialNumber)
        {
            return FurnitureDAL.getItemBySerialNumber(serialNumber);
        }
    }
}
