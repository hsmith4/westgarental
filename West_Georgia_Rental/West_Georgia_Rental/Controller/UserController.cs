﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using West_Georgia_Rental.DAL;
using West_Georgia_Rental.Model;

namespace West_Georgia_Rental.Controller
{
    class UserController
    {
        //Calls DAL to get user with name and password
        public User GetUser(string username, string password)
        {
            return UserDAL.getUserAtLogin(username, password);
        }

        //Calls DAL to get password for passed user
        public String getPassword(string username)
        {
            return UserDAL.getPassword(username);
        }

        //Calls DAL to update password with passed password for passed user
        public void updatePassword(string username, string password)
        {
            UserDAL.updatePassword(username, password);
        }
    }
}
