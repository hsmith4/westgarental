﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using West_Georgia_Rental.Controller;
using West_Georgia_Rental.Model;

namespace West_Georgia_Rental.View
{
    public partial class FormMemberManagement : Form
    {
        //Form variables
        private static FormMemberManagement instance;
        private static FormReturnHistory returnHistory;
        private static FormAddMember frmAddMember;
        private static FormEditMember frmEditMember;
        private static FormReturnOrder frmReturn;
        private FormNewRentalForSelectedMember frmNewRental;
        private FormSearchInventory searchInventory;
        private FormViewCart cartView;
        
        //Controller variables
        private MemberController memberController;
        private RentalLineItemController rliController;
        private CartController cartController;

        //Other variables
        private List<Member> memberListing;
        private Member selectedMember;
        private string phone;
        private string fName;
        private string lName;

    /****** CONSTRUCTORS *****/

        //This version is opened when the user opens the management page from the employee home page
        public FormMemberManagement()
        {
            instance = this;
            memberController = new MemberController();
            rliController = new RentalLineItemController();
            cartController = new CartController();
            memberListing = new List<Member>();
            InitializeComponent();
            btnRentForMember.Visible = false;
        }

        //This version is opened if the user tries to add an item to their cart before selecting a member
        public FormMemberManagement(FormSearchInventory searchForm)
        {
            InitializeComponent();
            instance = this;

            memberController = new MemberController();
            rliController = new RentalLineItemController();
            cartController = new CartController();

            memberListing = new List<Member>();
            this.searchInventory = searchForm;
            groupBox1.Visible = false;
            groupBox2.Visible = false;
        }

        //Returns current instance of this form
        public static FormMemberManagement Instance
        {
            get
            {
                return instance;
            }
        }

    /***** BUTTON HANDLERS *****/

        //Shows the ADD MEMBER FORM
        //including an instance of this form in order to update the data grid view on close
        private void btnAddMember_Click(object sender, EventArgs e)
        {
            try
            {
                if (frmAddMember == null)
                {
                    frmAddMember = new FormAddMember(this);
                    frmAddMember.MdiParent = FormMainParent.Instance;
                    frmAddMember.FormClosed += FrmAddMember_FormClosed;
                    frmAddMember.StartPosition = FormStartPosition.CenterScreen;
                    frmAddMember.Show();
                }
                else
                {
                    frmAddMember.Activate();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.GetType().ToString());
            }
        }
        
        //Add member form set to null on close
        private void FrmAddMember_FormClosed(object sender, FormClosedEventArgs e)
        {
            frmAddMember = null;
        }

        //Checks for a single member to be saved
        //then EDIT MEMBER FORM is shown
        //including an instance of this form in order to update the data grid view on close
        private void btnEditMember_Click(object sender, EventArgs e)
        {
            if (dataGridView1.SelectedRows.Count != 1)
                MessageBox.Show("Please select a single member to edit.");
            else if (frmEditMember == null)
            {
                try
                {
                    frmEditMember = new FormEditMember(this);
                    frmEditMember.MdiParent = FormMainParent.Instance;
                    frmEditMember.FormClosed += FrmEditMember_FormClosed;
                    frmEditMember.StartPosition = FormStartPosition.CenterScreen;
                    frmEditMember.Show();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, ex.GetType().ToString());
                }
            }
            else
                frmAddMember.Activate();
        }

        //Edit member form set to null on close
        private void FrmEditMember_FormClosed(object sender, FormClosedEventArgs e)
        {
            frmEditMember = null;
        }

        //Displays confirmation message and DELETES MEMBER
        //Updates the data grid view 
        private void btnDeleteMember_Click(object sender, EventArgs e)
        {
            DialogResult result = MessageBox.Show("Are you sure you want to delete " + selectedMember.fName + " " + selectedMember.lName + "?", "Confirm Delete", MessageBoxButtons.YesNo);
            if (result == DialogResult.Yes)
            {
                int rowCount = this.memberController.DeleteMember(this.selectedMember.memberID);
                MessageBox.Show(selectedMember.fName + " " + selectedMember.lName + " deleted successfully.");
                clearGridView();
            }
        }

        //Opens the RENTAL and RETURN FORM
        private void returnOnClick(object sender, EventArgs e)
        {
            if (this.selectedMember == null)
            {
                MessageBox.Show("Please select a member");
                return;
            }
            try
            {
                int rentals = rliController.getRentalsByID(getSelectedMember().memberID).Count();

                if (dataGridView1.SelectedRows.Count != 1)
                {
                    MessageBox.Show("Please select a single member.");
                }

                if (rentals == 0)
                {
                    MessageBox.Show(getSelectedMember().fName + " " + getSelectedMember().lName + " does not have any open orders");
                    //return;
                }
                //else if (frmEditMember == null)
                if (frmReturn == null)
                {
                    try
                    {
                        frmReturn = new FormReturnOrder(this);
                        frmReturn.MdiParent = FormMainParent.Instance;
                        frmReturn.FormClosed += FrmReturnOrder_FormClosed;
                        frmReturn.StartPosition = FormStartPosition.CenterScreen;
                        frmReturn.Show();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message, ex.GetType().ToString());
                    }
                }
                else
                    frmReturn.Activate();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        //Return form set to null on close
        private void FrmReturnOrder_FormClosed(object sender, FormClosedEventArgs e)
        {
            frmReturn.Dispose();
            frmReturn = null;
            this.Show();
        }

        //Opens the selected member's CART
        private void memberCart_Click(object sender, EventArgs e)
        {
            if (dataGridView1.SelectedRows.Count != 1)
            {
                MessageBox.Show("Please select a single member.");
            }
            else if (this.cartController.GetCountOfMemberCart(selectedMember.memberID) < 1)
            {
                MessageBox.Show(this.selectedMember.fName + " " + this.selectedMember.lName + " does not have any items in their cart.");
            }
            else if (cartView == null)
            {
                try
                {
                    cartView = new FormViewCart(this);
                    cartView.MdiParent = FormMainParent.Instance;
                    cartView.FormClosed += CartView_FormClosed;
                    cartView.StartPosition = FormStartPosition.CenterScreen;
                    cartView.Show();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, ex.GetType().ToString());
                }
            }
            else
                cartView.Activate();
        }
        
        //Cart form set to null on close
        private void CartView_FormClosed(object sender, FormClosedEventArgs e)
        {
            cartView.Dispose();
            cartView = null;
        }

        //Opens the rental confirmation form
        //Only available when the user has already chosen an item to rent (and not selected a member first)
        private void btnRentForMember_Click(object sender, EventArgs e)
        {
            if (dataGridView1.SelectedRows.Count != 1)
            {
                MessageBox.Show("Please select a single member.");
            }
            else if (frmNewRental == null)
            {
                try
                {
                    frmNewRental = new FormNewRentalForSelectedMember(this, searchInventory);
                    frmNewRental.MdiParent = FormMainParent.Instance;
                    frmNewRental.FormClosed += FrmNewRental_FormClosed;
                    frmNewRental.StartPosition = FormStartPosition.CenterScreen;
                    //this.Hide();
                    frmNewRental.Show();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, ex.GetType().ToString());
                }
            }
            else
            {
                frmNewRental.Activate();
            }
        }

        //New rental form to null on close
        private void FrmNewRental_FormClosed(object sender, FormClosedEventArgs e)
        {
            frmNewRental = null;
        }

        //Closes the member management form
        private void btnBack_Click(object sender, EventArgs e)
        {
            this.Close();
            if (this.searchInventory != null)
            {
                this.searchInventory.setMemberManagementForm(null);
            }
        }


    /***** VALIDATION *****/

        //Validates search criteria and shows matching members
        private void getMemberOnClick(object sender, EventArgs e)
        {
            try
            {
                this.getMemberByPhoneOrName();
                if (memberListing.Count == 0)
                {
                    MessageBox.Show("No members found by that criteria");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.GetType().ToString());
            }
        }

        //Populates gridview based on user input if validation passes. Appropriate text box explains error
        //if invalid data is provided
        public void getMemberByPhoneOrName()
        {
            this.phone = phoneText1.Text + phoneText2.Text + phoneText3.Text;
            this.fName = firstNameText.Text;
            this.lName = lastNameText.Text;

            if (phone.Equals("") && fName.Equals("") && lName.Equals(""))
            {
                MessageBox.Show("Please enter a phone number or first and last name");
            }
            else if (fName.Equals("") && lName.Equals("") && phone.Length != 10)
            {
                MessageBox.Show("Invalid phone number. Please try again");
            }
            else if (!fName.Equals("") && lName.Equals(""))
            {
                MessageBox.Show("Please provide a last name");
            }
            else if (fName.Equals("") && !lName.Equals(""))
            {
                MessageBox.Show("Please provide a first name");
            }
            else if (fName.Equals("") && lName.Equals("") && phone.Length == 10)
            {
                try
                {
                    int phoneNumber = (int)Int64.Parse(phone);

                    if (phoneNumber < 0)
                    {
                        MessageBox.Show("Phone number can only contain positive numbers");
                        return;
                    }
                }
                catch (Exception)
                {
                    MessageBox.Show("Phone number can not have letters or special characters");
                    return;
                }
                memberListing = memberController.getMemberByPhoneOrName(phone, fName, lName);
                dataGridView1.DataSource = memberListing;
            }
            else if (!fName.Equals("") && !lName.Equals("") && phone.Length == 0)
            {
                memberListing = memberController.getMemberByPhoneOrName(phone, fName, lName);
                dataGridView1.DataSource = memberListing;
            }
            else
            {
                try
                {
                    int phoneNumber = (int)Int64.Parse(phone);
                
                    if (phoneNumber < 0)
                    {
                        MessageBox.Show("Phone number can only contain positive numbers");
                        return;
                    }
                }
                catch (Exception)
                {
                    MessageBox.Show("Phone number can not have letters or special characters");
                    return;
                }
                memberListing = memberController.getMemberByPhoneOrName(phone, fName, lName);
                dataGridView1.DataSource = memberListing;
            }
        }

        //Concats the three phone text boxes into a single string
        public string getPhone()
        {
            return phoneText1.Text + phoneText2.Text + phoneText3.Text;
        }

        //Returns string for first name text box
        public string getFName()
        {
            return firstNameText.Text;
        }

        //Return string for last name text box
        public string getLName()
        {
            return lastNameText.Text;
        }
        
    /***** MEMBER SEARCH AND SELECTION *****/

        //member info is saved ONLY if a single member is selected
        private void dataGridView1_SelectionChanged(object sender, EventArgs e)
        {

            if (dataGridView1.SelectedRows.Count == 1)
            {
                int memberID = Convert.ToInt32(dataGridView1.SelectedRows[0].Cells[0].Value);
                this.selectedMember = this.memberController.GetMemberById(memberID);
            }
        }

        //Gets the member info that is saved when member is selected
        public Member getSelectedMember()
        {
            return this.selectedMember;
        }

        //Clears the member search selection criteria
        private void clearOnClick(object sender, EventArgs e)
        {
            phoneText1.Text = "";
            phoneText2.Text = "";
            phoneText3.Text = "";
            firstNameText.Text = "";
            lastNameText.Text = "";
            memberListing = null;
            dataGridView1.DataSource = memberListing;
        }

        //Clears the member search data grid view
        public void clearGridView()
        {
            memberListing = null;
            dataGridView1.DataSource = memberListing;
        }

        //Displays members returned in search
        private void showAllMembersOnClick(object sender, EventArgs e)
        {
            memberListing = memberController.getAllMembers();
            dataGridView1.DataSource = memberListing;
        }

        //Show updated member list in gridview
        public void showAllMembersAfterUpdate()
        {
            memberListing = memberController.getAllMembers();
            dataGridView1.DataSource = memberListing;
        }

        //Returns member listing
        public List<Member> getMemberList()
        {
            return this.memberListing;
        }

        //Returns gridview
        public DataGridView getGridView()
        {
            return dataGridView1;
        }

        //Creates and shows new instance of return history form when button clicked
        private void returnHistoryOnClick(object sender, EventArgs e)
        {
            try
            {
                if (returnHistory == null)
                {
                    returnHistory = new FormReturnHistory(this);
                    returnHistory.MdiParent = FormMainParent.Instance;
                    returnHistory.FormClosed += new FormClosedEventHandler(ReturnHistory_FormClosed);
                    returnHistory.StartPosition = FormStartPosition.CenterScreen;
                    this.Hide();
                    returnHistory.Show();
                }
                else
                {
                    returnHistory.Activate();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        //Set return history form to null on close. This form is reopened
        private void ReturnHistory_FormClosed(object sender, FormClosedEventArgs e)
        {
            returnHistory.Dispose();
            returnHistory = null;
            this.Show();
        }

        //Sets member management form to null on close
        private void FormMemberManagement_FormClosed(object sender, FormClosedEventArgs e)
        {
            if (this.searchInventory != null)
            {
                this.searchInventory.setMemberManagementForm(null);
            }
        }
    }
}
