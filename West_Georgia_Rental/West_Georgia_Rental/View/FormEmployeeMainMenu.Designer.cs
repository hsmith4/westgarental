﻿namespace West_Georgia_Rental.View
{
    partial class FormEmployeeMainMenu
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormEmployeeMainMenu));
            this.label1 = new System.Windows.Forms.Label();
            this.inventoryButton = new System.Windows.Forms.Button();
            this.memberButton = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.passwordBtn = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.reportButton = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Georgia", 22F);
            this.label1.Location = new System.Drawing.Point(37, 40);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(93, 35);
            this.label1.TabIndex = 0;
            this.label1.Text = "label1";
            this.label1.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // inventoryButton
            // 
            this.inventoryButton.Location = new System.Drawing.Point(55, 326);
            this.inventoryButton.Name = "inventoryButton";
            this.inventoryButton.Size = new System.Drawing.Size(75, 23);
            this.inventoryButton.TabIndex = 2;
            this.inventoryButton.Text = "Select";
            this.inventoryButton.UseVisualStyleBackColor = true;
            this.inventoryButton.Click += new System.EventHandler(this.searchOnClick);
            // 
            // memberButton
            // 
            this.memberButton.Location = new System.Drawing.Point(55, 369);
            this.memberButton.Name = "memberButton";
            this.memberButton.Size = new System.Drawing.Size(75, 23);
            this.memberButton.TabIndex = 3;
            this.memberButton.Text = "Select";
            this.memberButton.UseVisualStyleBackColor = true;
            this.memberButton.Click += new System.EventHandler(this.memberButton_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Georgia", 20F);
            this.label2.Location = new System.Drawing.Point(153, 318);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(408, 31);
            this.label2.TabIndex = 4;
            this.label2.Text = "Search and View Store Inventory";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Georgia", 20F);
            this.label3.Location = new System.Drawing.Point(153, 361);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(358, 31);
            this.label3.TabIndex = 5;
            this.label3.Text = "Member and Rental Services";
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(309, 91);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(401, 206);
            this.pictureBox1.TabIndex = 6;
            this.pictureBox1.TabStop = false;
            // 
            // passwordBtn
            // 
            this.passwordBtn.Location = new System.Drawing.Point(55, 412);
            this.passwordBtn.Name = "passwordBtn";
            this.passwordBtn.Size = new System.Drawing.Size(75, 23);
            this.passwordBtn.TabIndex = 7;
            this.passwordBtn.Text = "Select";
            this.passwordBtn.UseVisualStyleBackColor = true;
            this.passwordBtn.Click += new System.EventHandler(this.changePasswordOnClick);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Georgia", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(153, 404);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(227, 31);
            this.label4.TabIndex = 8;
            this.label4.Text = "Change Password";
            // 
            // reportButton
            // 
            this.reportButton.Location = new System.Drawing.Point(55, 455);
            this.reportButton.Name = "reportButton";
            this.reportButton.Size = new System.Drawing.Size(75, 23);
            this.reportButton.TabIndex = 9;
            this.reportButton.Text = "Select";
            this.reportButton.UseVisualStyleBackColor = true;
            this.reportButton.Click += new System.EventHandler(this.getReportOnClick);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Georgia", 20F);
            this.label5.Location = new System.Drawing.Point(153, 447);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(328, 31);
            this.label5.TabIndex = 10;
            this.label5.Text = "View Popular Item Report";
            // 
            // FormEmployeeMainMenu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Window;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.ClientSize = new System.Drawing.Size(974, 511);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.reportButton);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.passwordBtn);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.memberButton);
            this.Controls.Add(this.inventoryButton);
            this.Controls.Add(this.label1);
            this.Name = "FormEmployeeMainMenu";
            this.Text = "Employee Home";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button inventoryButton;
        private System.Windows.Forms.Button memberButton;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Button passwordBtn;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button reportButton;
        private System.Windows.Forms.Label label5;
    }
}