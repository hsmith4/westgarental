﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using West_Georgia_Rental.Controller;
using West_Georgia_Rental.Model;

namespace West_Georgia_Rental.View
{
    public partial class FormReturnOrder : Form
    {
        private static FormReturnOrder instance;
        private FormMemberManagement mmForm;
        private FormConfirmReturn confirmReturn;
        private FormSearchInventory searchInventory;
        private FormConfirmBulkReturn confirmBulkReturn;

        private RentalLineItemController rliController;

        private List<Rental> rentalList;
        private Rental selectedRental;
        private List<Rental> itemsToReturn;
        
        //private FormReturnHistory returnHistory;

        public FormReturnOrder(FormMemberManagement mmForm)
        {
            instance = this;
            InitializeComponent();
            rliController = new RentalLineItemController();
            this.mmForm = mmForm;
            this.itemsToReturn = new List<Rental>();
            setLabel();
            showOpenRentals();
        }

        //Returns current instance of this form
        public static FormReturnOrder Instance
        {
            get
            {
                return instance;
            }
        }

        //Initializes labels to the defined string
        private void setLabel()
        {
            label1.Text = "Open rentals for " + mmForm.getSelectedMember().fName + " " + mmForm.getSelectedMember().lName;
            
        }

        //Helper that fills gridview with specified data
        private void showOpenRentals()
        {
            rentalList = rliController.getRentalsByID(mmForm.getSelectedMember().memberID);
            dataGridView1.DataSource = rentalList;
            dataGridView1.CellFormatting += DataGridView1_CellFormatting;
        }

        //Helper to format the gridview
        private void DataGridView1_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            if (dataGridView1.Columns[e.ColumnIndex].Name == "Status")
            {
                string status = this.rliController.getRentalStatus(Convert.ToInt32(dataGridView1.Rows[e.RowIndex].Cells[0].Value), Convert.ToInt32(dataGridView1.Rows[e.RowIndex].Cells[1].Value));
                e.Value = status;
            }
        }

        //Closes the form without doing anything
        private void cancelOnClick(object sender, EventArgs e)
        {
            this.Close();
        }

        //Verified at least one item is selected. Opens the appropriate form depending on how many items
        //are selected
        private void returnOnClick(object sender, EventArgs e)
        {
            if (dataGridView1.SelectedRows.Count > 1)
            {
                if (confirmBulkReturn == null)
                {
                    try
                    {
                        confirmBulkReturn = new FormConfirmBulkReturn(this);
                        confirmBulkReturn.MdiParent = FormMainParent.Instance;
                        confirmBulkReturn.FormClosed += ConfirmBulkReturn_FormClosed;
                        confirmBulkReturn.StartPosition = FormStartPosition.CenterScreen;
                        //this.Hide();
                        confirmBulkReturn.Show();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message, ex.GetType().ToString());
                    }
                }
                else
                    confirmBulkReturn.Activate();
            }
            else if (dataGridView1.SelectedRows.Count == 1)
            {
                if (confirmReturn == null)
                {
                    try
                    {
                        confirmReturn = new FormConfirmReturn(this);
                        confirmReturn.MdiParent = FormMainParent.Instance;
                        confirmReturn.FormClosed += FrmCorfirmReturn_FormClosed;
                        confirmReturn.StartPosition = FormStartPosition.CenterScreen;
                        //this.Hide();
                        confirmReturn.Show();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message, ex.GetType().ToString());
                    }
                }
                else
                    confirmReturn.Activate();
            }
            else if (dataGridView1.SelectedRows.Count != 1)
            {
                MessageBox.Show("Please select a single item to return.");
            }
        }

        //Sets bulk return form to null on close
        private void ConfirmBulkReturn_FormClosed(object sender, FormClosedEventArgs e)
        {
            confirmBulkReturn.Dispose();
            confirmBulkReturn = null;
            showOpenRentals();
            //this.Show(); ;
        }

        //Sets single return form to null on close
        private void FrmCorfirmReturn_FormClosed(object sender, FormClosedEventArgs e)
        {
            confirmReturn.Dispose();
            confirmReturn = null;
            showOpenRentals();
            //this.Show();
        }

        //Sets selected rental to selected value anytime selection is changed
        private void dataGridView1_SelectionChanged(object sender, EventArgs e)
        {
            if (dataGridView1.SelectedRows.Count == 1)
            {
                int rentalID = Convert.ToInt32(dataGridView1.SelectedRows[0].Cells[0].Value);
                int serialNumber = Convert.ToInt32(dataGridView1.SelectedRows[0].Cells[1].Value);
                this.selectedRental = this.rliController.getRentalItemBySerialNumber(rentalID, serialNumber);
            }
            else if (dataGridView1.SelectedRows.Count > 1)
            {
                foreach (DataGridViewRow row in dataGridView1.SelectedRows)
                {
                    Rental rental = new Rental();
                    rental.serialNumber = Convert.ToInt32(dataGridView1.SelectedRows[0].Cells[1].Value);
                    rental.rentalID = Convert.ToInt32(dataGridView1.SelectedRows[0].Cells[0].Value);
                    itemsToReturn.Add(rental);
                }
            }
        }

        //Gets the member info that is saved when member is selected
        public Rental getSelectedRental()
        {
            return this.selectedRental;
        }

        //Returns selected row(s)
        public DataGridViewSelectedRowCollection getSelectedRows()
        {
            return dataGridView1.SelectedRows;
        }

        //Returns selected member from the member management form
        public Member getMember()
        {
            return mmForm.getSelectedMember();
        }

        //Creates new instance of inventory search when button is clicked
        private void btnNewRental_Click(object sender, EventArgs e)
        {
            try
            {
                if (searchInventory == null)
                {
                    searchInventory = new FormSearchInventory(mmForm);
                    searchInventory.MdiParent = FormMainParent.Instance;
                    searchInventory.FormClosed += new FormClosedEventHandler(SearchInventory_FormClosed);
                    searchInventory.StartPosition = FormStartPosition.CenterScreen;
                    //this.Hide();
                    searchInventory.Show();
                }
                else
                {
                    searchInventory.Activate();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.GetType().ToString());
            }
            
        }

        //Sets inventory search to null on close
        private void SearchInventory_FormClosed(object sender, FormClosedEventArgs e)
        {
            searchInventory.Dispose();
            searchInventory = null;
            this.Show();
        }

        //Shows open rentals
        private void FormReturnOrder_Enter(object sender, EventArgs e)
        {
            this.showOpenRentals();
        }
    }
}
