﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;
using West_Georgia_Rental.Controller;

namespace West_Georgia_Rental.View
{
    public partial class FormAddMember : Form
    {
        private static FormAddMember instance;
        private FormMemberManagement mmForm;
        private MemberController memberController;

        //Initializes form with passed member management form
        public FormAddMember(FormMemberManagement mmForm)
        {
            instance = this;
            InitializeComponent();
            this.mmForm = mmForm;
            this.memberController = new MemberController();
            this.populateStateComboBox();
        }

        //Returns current instance of this form
        public static FormAddMember Instance
        {
            get
            {
                return instance;
            }
        }

        //Helper to populate state combo box
        private void populateStateComboBox()
        {
            string[] states = new string[] {"AL", "AK", "AZ", "AR", "CA", "CO", "CT", "DE", "FL", "GA", "HI", "ID",
                "IL", "IN", "IA", "KS", "KY", "LA", "ME", "MD", "MA", "MI", "MN", "MS", "MO", "MT", "NE",
                "NV", "NH", "NJ", "NM", "NY", "NC", "ND", "OH", "OK", "OR", "PA", "RI", "SC", "SD", "TN",
                "TX", "UT", "VT", "VA", "WA", "WV", "WI", "WY" };
            cbState.DataSource = states;
        }

        //Add new member if validation passes. If validation fails, message box informs user why
        private void btnAdd_Click(object sender, EventArgs e)
        {
            if (tbEmail.Text != "")
            {
                try
                {
                    MailAddress email = new MailAddress(tbEmail.Text);
                }
                catch (Exception)
                {
                    MessageBox.Show("Please enter a valid email address.");
                }
            }
                string fullPhone = tbPhone1.Text + tbPhone2.Text + tbPhone3.Text;
            if (tbFirstName.Text == "")
                MessageBox.Show("Please enter a first name.");
            else if (tbLastName.Text == "")
                MessageBox.Show("Please enter a last name.");
            else if (fullPhone.Length != 10)
                MessageBox.Show("Please enter a 10-digit phone number.");
            else if (tbEmail.Text == "")
                MessageBox.Show("Please enter an email address.");
            else if (tbAddress1.Text == "")
                MessageBox.Show("Please enter a street address.");
            else if (tbCity.Text == "")
                MessageBox.Show("Please enter a city.");
            else if (tbZip.Text == "" || tbZip.Text.Length != 5)
                MessageBox.Show("Please enter a 5-digit zip code"); 
            else
            {
                try
                {
                    int zip = Convert.ToInt32(tbZip.Text);

                    if (zip < 0)
                    {
                        MessageBox.Show("Zip code can only contain positive numbers");
                        return;
                    }
                }
                catch (Exception)
                {
                    MessageBox.Show("Zip code can not have letters or special characters");
                    return;
                }
                try
                {
                    int phoneNumber = (int)Int64.Parse(fullPhone);

                    if (phoneNumber < 0)
                    {
                        MessageBox.Show("Phone number can only contain positive numbers");
                        return;
                    }
                }
                catch (Exception)
                {
                    MessageBox.Show("Phone number can not have letters or special characters");
                    return;
                }
                try
                {
                    int success = this.memberController.AddMember(dtpDOB.Value, tbFirstName.Text, tbMinit.Text, tbLastName.Text, fullPhone, tbEmail.Text, tbAddress1.Text, tbAddress2.Text, tbCity.Text, cbState.Text, tbZip.Text);
                    if (success > 0)
                    {
                        mmForm.clearGridView();
                        MessageBox.Show("Member successfully added!");
                        this.Close();
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, ex.GetType().ToString());
                }
            }
        }

        //Closes the form without doing anything
        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
