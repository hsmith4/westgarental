﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using West_Georgia_Rental.Model;

namespace West_Georgia_Rental.View
{
    public partial class FormMainParent : Form
    {
        private static FormLogin loginForm;
        private static FormMainParent instance;
        private static FormEmployeeMainMenu employeeMenu;
        private static FormAddMember frmAddMember;
        private static FormMemberManagement frmMemberManagement;
        private static FormEditMember frmEditMember;
        private static FormSearchInventory frmSearchInventory;
        private static FormUpdatePassword frmUpdatePassword;
        private static FormReturnOrder frmReturnOrder;
        private static FormConfirmReturn frmConfirmReturn;
        private static FormConfirmBulkReturn frmBulkReturn;
        private static FormReturnHistory frmReturnHistory;
        private static FormViewCart frmViewCart;
        private static FormNewRentalForSelectedMember frmNewRental;
        private static FormReportParameters reportParameters;
        private static FormViewFurnitureReport viewReport;

        //Initializes form
        public FormMainParent()
        {
            instance = this;
            InitializeComponent();
            employeeMenu = null;
            this.showLoginScreen();
            WindowState = FormWindowState.Maximized;
        }

        //Sets login form to null on close. Menu items are selectable if user is not null
        private void LoginForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            loginForm = null;

            if (CurrentUser.User != null)
                this.setToolStripMenuItemsEnabled();
        }

        //Closes the application when "Exit" is clicked in menu
        private void exitOnClick(object sender, EventArgs e)
        {
            Application.Exit();
        }

        //Returns current instance of this form
        public static FormMainParent Instance
        {
            get
            {
                return instance;
            }
        }

        //Opens login form when "login" is clicked in menu
        private void loginToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.showLoginScreen();
        }

        //Creates and shows new instance of the login form
        private void showLoginScreen()
        {
            if (CurrentUser.User == null)
            {
                loginForm = new FormLogin();
                loginForm.MdiParent = this;
                loginForm.StartPosition = FormStartPosition.CenterScreen;
                loginForm.FormClosed += LoginForm_FormClosed;
                loginForm.Show();
            }
            else
                loginForm.Activate();
        }

        //Asks user to verify they want to logout when "logout" is clicked in menu. All active forms are
        //closed and user is set to null if user clicks "yes"
        private void logoutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            setFormVariables();

            DialogResult result = MessageBox.Show("Are you sure you want to logout, " + CurrentUser.User.username, "Logging out", MessageBoxButtons.YesNo);
            if (result == DialogResult.Yes)
            {
                CurrentUser.setCurrentUser(null);
                setToolStripMenuItemsEnabled();
                MessageBox.Show("Successfully logged out.");

                closeAllActiveForms();
                this.showLoginScreen();
            }
        }

        //Helper that sets all form variables to current instance if not null
        private void setFormVariables()
        {
            employeeMenu = FormEmployeeMainMenu.Instance;
            frmAddMember = FormAddMember.Instance;
            frmMemberManagement = FormMemberManagement.Instance;
            frmEditMember = FormEditMember.Instance;
            frmSearchInventory = FormSearchInventory.Instance;
            frmUpdatePassword = FormUpdatePassword.Instance;
            frmReturnOrder = FormReturnOrder.Instance;
            frmConfirmReturn = FormConfirmReturn.Instance;
            frmBulkReturn = FormConfirmBulkReturn.Instance;
            frmReturnHistory = FormReturnHistory.Instance;
            frmViewCart = FormViewCart.Instance;
            frmNewRental = FormNewRentalForSelectedMember.Instance;
            reportParameters = FormReportParameters.Instance;
            viewReport = FormViewFurnitureReport.Instance;
        }

        //Calls on helper to close all active forms before closing the employee menu form
        private void closeAllActiveForms()
        {
            ifActiveForm(frmAddMember);
            ifActiveForm(frmEditMember);
            ifActiveForm(frmSearchInventory);
            ifActiveForm(frmUpdatePassword);
            ifActiveForm(frmMemberManagement);
            ifActiveForm(frmReturnOrder);
            ifActiveForm(frmConfirmReturn);
            ifActiveForm(frmBulkReturn);
            ifActiveForm(frmReturnHistory);
            ifActiveForm(frmViewCart);
            ifActiveForm(frmNewRental);
            ifActiveForm(reportParameters);
            ifActiveForm(viewReport);
            
            employeeMenu.Close();
            employeeMenu = null;
        }

        //Helper that sets passed form to null if passed form is not null
        private void ifActiveForm(Form form)
        {
            if (form != null)
            {
                form.Dispose();
                form = null;
            }
        }

        //Sets menustrip to enabled/disabled based on user status
        private void setToolStripMenuItemsEnabled()
        {
            loginToolStripMenuItem.Enabled = !loginToolStripMenuItem.Enabled;
            logoutToolStripMenuItem.Enabled = !logoutToolStripMenuItem.Enabled;
        }
    }
}
