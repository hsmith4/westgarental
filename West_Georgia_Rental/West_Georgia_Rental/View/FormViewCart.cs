﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using West_Georgia_Rental.Controller;
using West_Georgia_Rental.Model;

namespace West_Georgia_Rental.View
{
    public partial class FormViewCart : Form
    {
        private FormMemberManagement mmForm;
        private FormSearchInventory searchInventory;
        private FormNewRentalForSelectedMember frmNewRental;
        private static FormViewCart instance;

        private CartController cartController;
        private RentalController rentController;
        private RentalLineItemController rliController;

        private List<Cart> cart;
        private Member selectedMember;
        private Cart selectedCartItem;

        //Initializes form with passed form as a parameter
        public FormViewCart(FormMemberManagement mmForm)
        {
            instance = this;
            InitializeComponent();
            this.mmForm = mmForm;

            this.cartController = new CartController();
            this.rentController = new RentalController();
            this.rliController = new RentalLineItemController();

            this.selectedMember = mmForm.getSelectedMember();
            this.cart = this.cartController.GetCartByMemberID(selectedMember.memberID);
            this.lblMember.Text = "Viewing cart for: " + selectedMember.fName + " " + selectedMember.lName;
            this.lblTotal.Text = "Cart Total: " + cartController.GetCartTotalForMember(selectedMember.memberID); 
        }

        //Returns current instance of this form
        public static FormViewCart Instance
        {
            get
            {
                return instance;
            }
        }


    /***** POPULATE CART *****/
        //Set data source and cell formatting
        private void FormViewCart_Load(object sender, EventArgs e)
        {
            //cartDataGridView.DataSource = this.cart;
            //cartDataGridView.CellFormatting += CartDataGridView_CellFormatting;
            this.populateCartDataGridView();
        }

        //Populates cart gridview with specified data
        public void populateCartDataGridView()
        {
            cartDataGridView.DataSource = this.cart;
            cartDataGridView.CellFormatting += CartDataGridView_CellFormatting;
        }
        //Cell format to populate the total cost for each item in the cart
        private void CartDataGridView_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            if (cartDataGridView.Columns[e.ColumnIndex].Name == "totalCost")
            {
                decimal total = this.cartController.GetItemTotal(Convert.ToInt32(cartDataGridView.Rows[e.RowIndex].Cells[0].Value));
                e.Value = "$" + total.ToString();
            }
            if (cartDataGridView.Columns[e.ColumnIndex].Name == "returnDate")
            {
                DateTime returnDate = this.cartController.getItemReturnDate(mmForm.getSelectedMember().memberID, Convert.ToInt32(cartDataGridView.Rows[e.RowIndex].Cells[0].Value));

                e.Value = returnDate.ToString("MM/dd/yyyy");
            }
        }

    /***** BUTTON HANDLERS *****/

        //Confirms rental of all items in the cart
        //Creates rental record
        //Creates rentalLineItem records for all items in the current member's cart
        //Deletes all items from the cart after creating line items
        private void btnConfirmRental_Click(object sender, EventArgs e)
        {
            DialogResult result = MessageBox.Show("Confirm rental for " + selectedMember.fName + " " + selectedMember.lName + "?", "Confirm Rental", MessageBoxButtons.YesNo);
            if (result == DialogResult.Yes)
            {
                try
                {
                    int rentalID = rentController.CreateRental(selectedMember.memberID, CurrentUser.User.empID);
                    foreach (Cart currentCart in this.cart)
                    {
                        if (this.rliController.createLineItem(rentalID, currentCart.serialNumber, selectedMember.memberID) == 1)
                        {
                            this.cartController.DeleteCartItem(selectedMember.memberID, currentCart.serialNumber);
                            
                        }
                        else
                        {
                            MessageBox.Show("Rental not successful.  Please try again.");
                            return;
                        } 
                    }
                    this.Close();
                    MessageBox.Show("Cart successfully rented!");
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, ex.GetType().ToString());
                }
            }
        }

        //Deletes the currently selected item from the cart
        private void btnRemoveItem_Click(object sender, EventArgs e)
        {
            try
            {
                cartController.DeleteCartItem(selectedMember.memberID, selectedCartItem.serialNumber);
                this.cart = this.cartController.GetCartByMemberID(selectedMember.memberID);
                cartDataGridView.DataSource = this.cart;
                cartDataGridView.CellFormatting += CartDataGridView_CellFormatting;

                if (this.cart.Count == 0)
                {
                    this.lblTotal.Text = "Cart Total: 0.00";
                    return;
                }
                else
                {
                    this.lblTotal.Text = "Cart Total: " + cartController.GetCartTotalForMember(selectedMember.memberID);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.GetType().ToString());
            }
        }

        //Closes this form without doing anything
        private void btnBack_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        //Returns (or takes) member to the inventory search page to shop for current member
        private void btnContinueShopping_Click(object sender, EventArgs e)
        {
            this.Close();
            try
            {
                if (searchInventory == null)
                {
                    searchInventory = new FormSearchInventory(mmForm);
                    searchInventory.MdiParent = FormMainParent.Instance;
                    searchInventory.FormClosed += SearchInventory_FormClosed;
                    searchInventory.StartPosition = FormStartPosition.CenterScreen;
                    //this.Hide();
                    searchInventory.Show();
                }
                else
                {
                    searchInventory.Activate();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.GetType().ToString());
            }
        }

        //Sets searchInventory to null when closed
        private void SearchInventory_FormClosed(object sender, FormClosedEventArgs e)
        {
            searchInventory = null;
        }

        //Ensures only one item is selected. Opens form for selected item if only one item is selected
        private void btnEditItem_Click(object sender, EventArgs e)
        {
            if (cartDataGridView.SelectedRows.Count == 1)
            {
                int serialNumber = Convert.ToInt32(cartDataGridView.SelectedRows[0].Cells[0].Value);
                this.selectedCartItem = this.cartController.GetCartItemBySerialNumber(serialNumber);
            }
            if (frmNewRental == null)
            {
                try
                {
                    frmNewRental = new FormNewRentalForSelectedMember(mmForm, this);
                    frmNewRental.MdiParent = FormMainParent.Instance;
                    frmNewRental.FormClosed += FrmNewRental_FormClosed;
                    frmNewRental.StartPosition = FormStartPosition.CenterScreen;
                    //this.Hide();
                    frmNewRental.Show();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, ex.GetType().ToString());
                }
            }
            else
            {
                frmNewRental.Activate();
            }
        }

        //Sets frmNewRental to null when closed
        private void FrmNewRental_FormClosed(object sender, FormClosedEventArgs e)
        {
            frmNewRental = null;
        }

    /***** CART ITEM SELECTION *****/

        //cart item is saved ONLY if a single item is selected
        private void dataGridView1_SelectionChanged(object sender, EventArgs e)
        {
            if (cartDataGridView.SelectedRows.Count == 1)
            {
                int serialNumber = Convert.ToInt32(cartDataGridView.SelectedRows[0].Cells[0].Value);
                this.selectedCartItem = this.cartController.GetCartItemBySerialNumber(serialNumber);
            }
        }

        //Returns the current selected item
        public Cart getSelectedItem()
        {
            return this.selectedCartItem;
        }
    }
}
