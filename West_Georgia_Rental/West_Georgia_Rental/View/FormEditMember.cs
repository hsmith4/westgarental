﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using West_Georgia_Rental.Controller;
using West_Georgia_Rental.Model;

namespace West_Georgia_Rental.View
{
    public partial class FormEditMember : Form
    {
        private FormMemberManagement mmForm;
        private MemberController memberController;
        private static FormEditMember instance;

        //Initializes form with passed member management form
        public FormEditMember(FormMemberManagement mmForm)
        {
            instance = this;
            InitializeComponent();
            this.mmForm = mmForm;
            this.memberController = new MemberController();
            this.populateStateComboBox();
            this.populateMemberInfo();

        }

        //Returns current instance of this form
        public static FormEditMember Instance
        {
            get
            {
                return instance;
            }
        }

        //Helper to populate state combo box
        private void populateStateComboBox()
        {
            string[] states = new string[] {"AL", "AK", "AZ", "AR", "CA", "CO", "CT", "DE", "FL", "GA", "HI", "ID",
                "IL", "IN", "IA", "KS", "KY", "LA", "ME", "MD", "MA", "MI", "MN", "MS", "MO", "MT", "NE",
                "NV", "NH", "NJ", "NM", "NY", "NC", "ND", "OH", "OK", "OR", "PA", "RI", "SC", "SD", "TN",
                "TX", "UT", "VT", "VA", "WA", "WV", "WI", "WY" };
            cbState.DataSource = states;
        }

        //Prepopulates all fields for selected member from member management form
        private void populateMemberInfo()
        {
            try
            { 
                Member selectedMember = mmForm.getSelectedMember();
                tbMemberID.Text = selectedMember.memberID.ToString();
                tbFirstName.Text = selectedMember.fName;
                tbMinit.Text = selectedMember.minit;
                tbLastName.Text = selectedMember.lName;
                dtpDOB.Value = Convert.ToDateTime(selectedMember.DOB.ToShortDateString());
                tbPhone1.Text = selectedMember.phone.Substring(0, 3);
                tbPhone2.Text = selectedMember.phone.Substring(3, 3);
                tbPhone3.Text = selectedMember.phone.Substring(6);
                tbEmail.Text = selectedMember.email;
                tbAddress1.Text = selectedMember.address1;
                tbAddress2.Text = selectedMember.address2;
                tbCity.Text = selectedMember.city;
                cbState.Text = selectedMember.state;
                tbZip.Text = selectedMember.zip;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.GetType().ToString());
            }
        }

        //Updates member information if all fields pass validation. Failing validation prompts the 
        //appropriate message box
        private void btnEdit_Click(object sender, EventArgs e)
        {
            string fullPhone = tbPhone1.Text + tbPhone2.Text + tbPhone3.Text;
            if (tbFirstName.Text == "")
                MessageBox.Show("Please enter first name.");
            else if (tbLastName.Text == "")
                MessageBox.Show("Please enter last name.");
            else if (fullPhone.Length != 10)
                MessageBox.Show("Please enter a 10-digit phone number.");
            else if (tbEmail.Text == "")
                MessageBox.Show("Please enter a valid email address.");
            else if (tbAddress1.Text == "")
                MessageBox.Show("Please enter a street address.");
            else if (tbCity.Text == "")
                MessageBox.Show("Please enter a city.");
            else if (tbZip.Text == "" || tbZip.Text.Length != 5)
                MessageBox.Show("Please enter a 5-digit zip code");
            else
            {
                try
                {
                    int zip = Convert.ToInt32(tbZip.Text);

                    if (zip < 0)
                    {
                        MessageBox.Show("Zip code can only contain positive numbers");
                        return;
                    }
                }
                catch (Exception)
                {
                    MessageBox.Show("Zip code can not have letters or special characters");
                    return;
                }
                try
                {
                    int phoneNumber = (int)Int64.Parse(fullPhone);

                    if (phoneNumber < 0)
                    {
                        MessageBox.Show("Phone number can only contain positive numbers");
                        return;
                    }
                }
                catch (Exception)
                {
                    MessageBox.Show("Phone number can not have letters or special characters");
                    return;
                }
                try
                {
                    if (this.memberController.UpdateMember(Convert.ToInt32(tbMemberID.Text), tbFirstName.Text, tbMinit.Text, tbLastName.Text, dtpDOB.Value, fullPhone, tbEmail.Text, tbAddress1.Text, tbAddress2.Text, tbCity.Text, cbState.Text, tbZip.Text))
                    {
                        MessageBox.Show("Member update successful!");
                        if ((this.mmForm.getFName() != "" && this.mmForm.getLName() != "") || this.mmForm.getPhone() != "")
                            mmForm.getMemberByPhoneOrName();
                        else
                            this.mmForm.showAllMembersAfterUpdate();
                        this.Close();
                    }
                    else
                        MessageBox.Show("Member update unsuccessful.  Please try again.");
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, ex.GetType().ToString());
                }
            }
        }

        //Closes this form without doing anything
        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
