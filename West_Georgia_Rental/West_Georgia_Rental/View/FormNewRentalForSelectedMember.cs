﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using West_Georgia_Rental.Controller;
using West_Georgia_Rental.Model;

namespace West_Georgia_Rental.View
{
    public partial class FormNewRentalForSelectedMember : Form
    {
        private static FormNewRentalForSelectedMember instance;
        private FormMemberManagement mmForm;
        private FormSearchInventory searchInventory;
        private FormViewCart cartForm;

        private CartController cartController;

        private FurnitureListing item;
        private Cart cartItem;

        //Initializes form to passed member management form and inventory search form
        public FormNewRentalForSelectedMember(FormMemberManagement mmForm, FormSearchInventory searchInventory)
        {
            instance = this;
            InitializeComponent();
            this.mmForm = mmForm;
            this.cartController = new CartController();
            this.searchInventory = searchInventory;
            this.item = searchInventory.getItem();
            this.cartItem = null;
            this.setLabels();
        }

        //Returns current instance of this form
        public static FormNewRentalForSelectedMember Instance
        {
            get
            {
                return instance;
            }
        }

        //Constructor used for updating cart item
        public FormNewRentalForSelectedMember(FormMemberManagement mmForm, FormViewCart viewCart)
        {
            instance = this;
            InitializeComponent();
            this.mmForm = mmForm;
            this.cartController = new CartController();
            this.cartForm = viewCart;
            this.cartItem = cartForm.getSelectedItem();
            this.item = null;
            this.setLabels();
            numericUpDown1.Value = this.cartItem.lengthOfRental;
        }

        //Helper to set labels
        private void setLabels()
        {
            try
            {
                if (this.item != null)
                    this.itemLabel.Text = "Selected Item: " + this.item.style + " " + this.item.description + " " + this.item.category;
                else
                    this.itemLabel.Text = "Selected Item: " + this.cartItem.styleName + " " + this.cartItem.description + " " + this.cartItem.categoryName;
                this.memberLabel.Text = "Selected Member: " + mmForm.getSelectedMember().fName + " " + mmForm.getSelectedMember().lName;
                this.totalLabel.Text = "Total rental cost: $0.00";
            }
            catch (Exception ex)
            {
                MessageBox.Show("No item seleted.  Please try again.");
            }
            
        }

        private void populateItemInfo()
        {
            
        }

        //Sets label to selected value
        private void numericUpDown1_ValueChanged(object sender, EventArgs e)
        {
            string actualRate = "";
            if (this.item != null)
                actualRate = this.item.dailyRate.Substring(1);
            else
                actualRate = this.cartItem.dailyRate.Substring(1);
            decimal rate = Convert.ToDecimal(actualRate);
            this.totalLabel.Text = "Total rental cost: " + (rate * numericUpDown1.Value);
        }

        //Closes this form without doing anything
        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        //If error checking passes then new cart item is created. Either the cart form or inventory search
        //is opened depending on how the user answers the dialog box on "continue shopping?"
        private void btnConfirm_Click(object sender, EventArgs e)
        {
            if (numericUpDown1.Value == 0 || numericUpDown1.Value > 365)
            {
                MessageBox.Show("Please enter a value between 1 and 365 days.");
            }
            else if (numericUpDown1.Value > 0 && numericUpDown1.Value < 365 && this.item != null)
            {
                int insertSuccess = this.cartController.InsertCart(mmForm.getSelectedMember().memberID, this.item.serialNumber, CurrentUser.User.empID, Convert.ToInt32(numericUpDown1.Value));
                if (insertSuccess > 0)
                {
                    DialogResult result = MessageBox.Show("Continue shopping?", "Cart successfully updated!", MessageBoxButtons.YesNo);
                    if (result == DialogResult.No)
                    {
                        this.Close();
                        try
                        {
                            cartForm = new FormViewCart(mmForm);
                            cartForm.MdiParent = FormMainParent.Instance;
                            cartForm.FormClosed += SearchInventory_FormClosed;
                            cartForm.StartPosition = FormStartPosition.CenterScreen;
                            cartForm.WindowState = FormWindowState.Normal;
                            cartForm.Show();
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show(ex.Message, ex.GetType().ToString());
                        }
                    }
                    else
                    {
                        this.Close();
                        this.mmForm.Hide();
                        this.searchInventory.setMemberManagementForm(mmForm);
                        searchInventory.Show();
                    }
                }
            }
            else if (this.cartController.GetCartItemBySerialNumber(cartForm.getSelectedItem().serialNumber) != null
                && numericUpDown1.Value > 0 && numericUpDown1.Value < 365)
            {
                int updateSuccess = this.cartController.UpdateCartItem(Convert.ToInt32(numericUpDown1.Value), this.cartForm.getSelectedItem().serialNumber);
                if (updateSuccess == 1)
                {
                    DialogResult result = MessageBox.Show("Cart successfully updated.");
                    this.cartForm.populateCartDataGridView();
                    this.Close();
                }
            }
            else
                MessageBox.Show("Please enter a value between 1 and 365 days.");
        }

        //Inventory search is set to null when closed
        private void SearchInventory_FormClosed(object sender, FormClosedEventArgs e)
        {
            cartForm = null;
        }
    }
}
