﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using West_Georgia_Rental.Controller;
using West_Georgia_Rental.Model;

namespace West_Georgia_Rental.View
{
    public partial class FormConfirmBulkReturn : Form
    {
        private FormReturnOrder returnForm;
        private static FormConfirmBulkReturn instance;
        private ReturnController returnController;
        private RentalLineItemController rliController;

        private List<Label> labels;
        private DataGridViewSelectedRowCollection rows;
        private List<Rental> newRows;

        //Initializes form with passed return form
        public FormConfirmBulkReturn(FormReturnOrder returnForm)
        {
            instance = this;
            InitializeComponent();
            this.returnForm = returnForm;
            this.returnController = new ReturnController();
            this.rliController = new RentalLineItemController();
            this.labels = new List<Label>();
            this.newRows = new List<Rental>();
            this.rows = this.returnForm.getSelectedRows();
        }

        //Returns current instance of this form
        public static FormConfirmBulkReturn Instance
        {
            get
            {
                return instance;
            }
        }

        //Get label to indicate whether a refund or fine is appropriate
        private String evaluateLabelForReturnValue(int index)
        {
            string message = "";

            if (getDateDifference(index) < 0)
            {
                int difference = (getDateDifference(index) * -1);
                double refund = difference * (Convert.ToDouble(rows[index].Cells[6].Value.ToString()));
                message = "Refund Amount: " + String.Format("${0:0.00}", refund);
            }
            else if (getDateDifference(index) == 0)
            {
                message = "Return on time. Thank you";
            }
            else
            {
                double fine = getDateDifference(index) * (Convert.ToDouble(rows[index].Cells[7].Value.ToString()));

                if (fine > 1000)
                {
                    fine = 1000;
                }
                message = "Fine Amount: " + String.Format("${0:0.00}", fine);
            }
            return message;
        }

        //Determine whether return is late or not
        // diff < 0 = refund
        // dif > 0 = fine
        // diff == 0 = on time
        private int getDateDifference(int index)
        {
            Return curreturn = new Return();
            curreturn.dueDate = (DateTime)rows[index].Cells[8].Value;
            int difference = (int)(DateTime.Now - curreturn.dueDate).TotalDays;
            return difference;
        }

        //populates the form with info from each selected item to return
        //and calculates the final total
        //all labels added to a list of labels and added to the form last
        private void FormConfirmBulkReturn_Load(object sender, EventArgs e)
        {
            int x = 25;
            int y = 25;
            decimal totalDiff = 0;
            decimal returnTotal = 0;

            //Populate labels for each selected item
            //First label with serialNumber and item info
            //Second label with total refund or fine for item
            for (int i = 0; i < this.returnForm.getSelectedRows().Count; i++)
            {
                //Create a rental with each row and add it to a list of rentals
                //because for some reason the rows are getting wiped after load.  
                //No idea why but this fix works!  :)
                this.newRows.Add(new Rental() { rentalID = Convert.ToInt32(this.returnForm.getSelectedRows()[i].Cells[0].Value), serialNumber = Convert.ToInt32(this.returnForm.getSelectedRows()[i].Cells[1].Value) });
                
                //Create labels and add them to the list to be added.
                this.labels.Add(new Label() { Text = "Serial #" + this.rows[i].Cells[1].Value.ToString() + ", " + this.rows[i].Cells[3].Value.ToString() + " " + this.rows[i].Cells[4].Value.ToString() + " " + this.rows[i].Cells[5].Value.ToString(), AutoSize = true, Font = new Font(this.Font.FontFamily, 12) });
                this.labels.Add(new Label() { Text = evaluateLabelForReturnValue(i), AutoSize = true, Font = new Font(this.Font.FontFamily, 12) });
                this.labels.Add(new Label() { Text = "--------" });
                totalDiff += getDateDifference(i);
                if (getDateDifference(i) > 0)
                {  
                    decimal lateItemFine = Convert.ToDecimal(evaluateLabelForReturnValue(i).Substring(evaluateLabelForReturnValue(i).IndexOf("$", StringComparison.Ordinal) + 1)) * -1;
                    returnTotal += lateItemFine;
                }
                else if (getDateDifference(i) < 0)
                {
                    returnTotal += Convert.ToDecimal(evaluateLabelForReturnValue(i).Substring(evaluateLabelForReturnValue(i).IndexOf("$", StringComparison.Ordinal) + 1));
                }
            }

            //Determine the final total label (refund or fine) for all items returned in bulk
            if (totalDiff < 0)
                this.labels.Add(new Label() { Text = "Total Return Refund: $" + returnTotal, AutoSize = true, Font = new Font(this.Font.FontFamily, 12, FontStyle.Bold) });
            else if (totalDiff > 0) 
                this.labels.Add(new Label() { Text = "Total Return Fine: $" + returnTotal * -1, AutoSize = true, Font = new Font(this.Font.FontFamily, 12, FontStyle.Bold) });
            else
                this.labels.Add(new Label() { Text = "All returned on time.  Thank you!", AutoSize = true, Font = new Font(this.Font.FontFamily, 12, FontStyle.Bold) });

            //Add all labels in order to the form at intervals of 25
            foreach (Label label in labels)
            {
                label.Left = x;
                label.Top = y;
                this.Controls.Add(label);
                y += 25;
            }
            
        }

        //Button to confirm the bulk return
        //Shows confirmation for each item returned.
        private void btnConfirmBulkReturn_Click(object sender, EventArgs e)
        {
            int empID = CurrentUser.User.empID;

            DialogResult dialogResult = MessageBox.Show("Are you sure you want to return these items?", "Confirm", MessageBoxButtons.YesNo);
            if (dialogResult == DialogResult.Yes)
            {
                try
                {
                    int returnID = returnController.returnLineItem(empID);
                    for (int i = 0; i < this.newRows.Count; i++)
                    {
                        //MessageBox.Show(i.ToString());
                        //MessageBox.Show(this.newRows[i].rentalID + " " + this.newRows[i].serialNumber);
                        if (rliController.updateLineItemRental(Convert.ToInt32(newRows[i].rentalID), returnID, Convert.ToInt32(newRows[i].serialNumber)))
                        {
                            MessageBox.Show("Item returned on " + DateTime.Now);
                           
                        }
                        else
                        {
                            MessageBox.Show("Item could not be returned");
                        }
                    }
                    this.Close();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
            else if (dialogResult == DialogResult.No)
            {
                return;
            }
        }

        //Closes form without doing anything
        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
