﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace West_Georgia_Rental.View
{
    public partial class FormViewFurnitureReport : Form
    {
        private static FormViewFurnitureReport instance;
        private static FormReportParameters reportParameters;

        //Initializes form to the passed form
        public FormViewFurnitureReport(FormReportParameters reportParameter)
        {
            instance = this;
            reportParameters = reportParameter;
            InitializeComponent();
        }

        //Returns the current instance of this form
        public static FormViewFurnitureReport Instance
        {
            get
            {
                return instance;
            }
        }

        //Show the specified report within the report viewer
        private void FormViewFurnitureReport_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the '_CS6232_G3DataSet1.member' table. You can move, or remove it, as needed.
            this.memberTableAdapter.Fill(this._CS6232_G3DataSet1.member, reportParameters.getCategory(),
                reportParameters.getStartDate(), reportParameters.getEndDate());

            this.reportViewer1.RefreshReport();
        }

        private void reportViewer1_Load(object sender, EventArgs e)
        {

        }
    }
}
