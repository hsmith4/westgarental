﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using West_Georgia_Rental.Controller;

namespace West_Georgia_Rental.View
{
    public partial class FormConfirmReturn : Form
    {
        private FormReturnOrder returnOrder;
        private FormEmployeeMainMenu employeeMenu;
        private static FormConfirmReturn instance;
        private ReturnController returnController;
        private RentalLineItemController rliController;

        //Initializes this form with passed order return form
        public FormConfirmReturn(FormReturnOrder returnOrder)
        {
            instance = this;
            InitializeComponent();
            employeeMenu = FormEmployeeMainMenu.Instance;
            returnController = new ReturnController();
            rliController = new RentalLineItemController();
            this.returnOrder = returnOrder;
            setLabels();
        }

        //Returns current instance of this form
        public static FormConfirmReturn Instance
        {
            get
            {
                return instance;
            }
        }

        //Sets labels to defined values
        private void setLabels()
        {
            label1.Text = "Item to be returned for " + returnOrder.getMember().fName + " " + returnOrder.getMember().lName;
            label2.Text = "Furniture Category: " + returnOrder.getSelectedRental().category;
            label3.Text = "Furniture Style: " + returnOrder.getSelectedRental().style;
            label4.Text = "Furniture Description: " + returnOrder.getSelectedRental().description;
            label5.Text = evaluateLabelForReturnValue();
        }

        //Helper that informs user of any fine/refund based on return date
        private String evaluateLabelForReturnValue()
        {
            string message = "";

            if (getDateDifference() < 0)
            {
                int difference = (getDateDifference() * -1);
                double refund = difference * (Convert.ToDouble(returnOrder.getSelectedRental().dailyRate));
                message = "Refund Amount: " + String.Format("${0:0.00}", refund);
            }
            else if (getDateDifference() == 0)
            {
                message = "Return on time. Thank you";
            }
            else
            {
                double fine = getDateDifference() * (Convert.ToDouble(returnOrder.getSelectedRental().dailyFine));

                if (fine > 1000)
                {
                    fine = 1000;
                }
                message = "Fine Amount: " + String.Format("${0:0.00}", fine);
            }
            return message;
        }

        //Returns days passed between rent and return
        private int getDateDifference()
        {
            int difference = (int)(DateTime.Now - returnOrder.getSelectedRental().dueDate).TotalDays;
            return difference;
        }

        //If user confirms return, return table is updated with item returned and returning employee
        private void confirmOnClick(object sender, EventArgs e)
        {
            int empID = employeeMenu.empID();

            DialogResult dialogResult = MessageBox.Show("Are you sure you want to return this item?", "Confirm", MessageBoxButtons.YesNo);
            if (dialogResult == DialogResult.Yes)
            {
                try
                {
                    int returnID = returnController.returnLineItem(empID);
                    if (rliController.updateLineItemRental(returnOrder.getSelectedRental().rentalID, returnID, returnOrder.getSelectedRental().serialNumber))
                    {
                        MessageBox.Show("Item returned on " + DateTime.Now);
                        this.Close();
                    }
                    else
                    {
                        MessageBox.Show("Item could not be returned");
                    }  
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
            else if (dialogResult == DialogResult.No)
            {
                return;
            }
        }

        //Closes form without doing anything
        private void cancelOnClick(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
