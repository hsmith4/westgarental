﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using West_Georgia_Rental.Controller;
using West_Georgia_Rental.Model;

namespace West_Georgia_Rental.View
{
    public partial class FormLogin : Form
    {
        private FormEmployeeMainMenu employeeMenu;
        private UserController userController;

        //Initializes form
        public FormLogin()
        {
            InitializeComponent();
            this.userController = new UserController();
            tbPassword.UseSystemPasswordChar = true;
        }

        //Logs in user if provided information is correct. Text boxes inform user with appropriate message
        //if log in attempt fails
        private void btnLogin_Click(object sender, EventArgs e)
        {
            try
            {
                byte[] data = System.Text.Encoding.ASCII.GetBytes(tbPassword.Text);
                data = new System.Security.Cryptography.SHA256Managed().ComputeHash(data);
                String hash = System.Text.Encoding.ASCII.GetString(data);

                CurrentUser.setCurrentUser(userController.GetUser(tbUsername.Text, hash));

                if (CurrentUser.User == null)
                    MessageBox.Show("Username and Password do not match.  Please try again.");
                else
                {
                    MessageBox.Show("Successful Login!");

                    this.Close();

                    employeeMenu = new FormEmployeeMainMenu();
                    employeeMenu.MdiParent = FormMainParent.Instance;
                    employeeMenu.FormClosed += new FormClosedEventHandler(EmployeeMenu_FormClosed);
                    employeeMenu.StartPosition = FormStartPosition.CenterScreen;

                    employeeMenu.Show();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.GetType().ToString());
            }
}
        //Employee menu set to null on close
        private void EmployeeMenu_FormClosed(object sender, FormClosedEventArgs e)
        {
            employeeMenu.Dispose();
            employeeMenu = null;
        }
    }
}
