﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using West_Georgia_Rental.Controller;
using West_Georgia_Rental.Model;

namespace West_Georgia_Rental.View
{
    public partial class FormReturnHistory : Form
    {
        private static FormReturnHistory instance;
        private FormMemberManagement frmMember;
        private List<Return> returns;
        private ReturnController returnController;

        //Initializes form with passed member management form
        public FormReturnHistory(FormMemberManagement frmMember)
        {
            instance = this;
            this.frmMember = frmMember;
            returns = new List<Return>();
            returnController = new ReturnController();
            InitializeComponent();
            label1.Text = frmMember.getSelectedMember().fName + "'s Return History";
            populateGridView();
        }

        //Returns current instance of this form
        public static FormReturnHistory Instance
        {
            get
            {
                return instance;
            }
        }

        //Populates gridview with defined data
        private void populateGridView()
        {
            returns = returnController.getReturnsByMember(frmMember.getSelectedMember().memberID);
            dataGridView1.DataSource = returns;
        }

        //Closes form without doing anything
        private void closeOnClick(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
