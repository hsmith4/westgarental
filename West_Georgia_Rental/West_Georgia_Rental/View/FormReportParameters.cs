﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using West_Georgia_Rental.Controller;
using West_Georgia_Rental.Model;

namespace West_Georgia_Rental.View
{
    public partial class FormReportParameters : Form
    {
        private static FormReportParameters instance;
        private CategoryController categoryController;
        private static FormViewFurnitureReport viewReport;
        private RentalController rentalController;

        //Initializes form
        public FormReportParameters()
        {
            instance = this;
            InitializeComponent();
            categoryController = new CategoryController();
            rentalController = new RentalController();
            populateCategoryComboBox();
            reset();
        }

        //Returns current instance of this form
        public static FormReportParameters Instance
        {
            get
            {
               return instance;
            }
        }

        //Populates combo box with defined listing
        private void populateCategoryComboBox()
        {
            List<FurnitureListing> allCategories = categoryController.getAllCategories();

            try
            {
                FurnitureListing aListing;

                for (int current = 0; current < allCategories.Count; current++)
                {
                    aListing = allCategories[current];
                    categoryBox.Items.Add(aListing.category);
                }
                categoryBox.SelectedItem = null;
            }
            catch (Exception)
            {
                MessageBox.Show("Category box could not be populated");
            }
        }

        //Returns selected category
        public string getCategory()
        {
            string category = categoryBox.SelectedItem.ToString();
            return category; 
        }

        //Returns selected start date
        public DateTime getStartDate()
        {
            return startDatePicker.Value;
        }

        //Returns selected end date
        public DateTime getEndDate()
        {
            return endDatePicker.Value;
        }

        //Closes form without doing anything
        private void closeOnClick(object sender, EventArgs e)
        {
            this.Close();
        }

        //Opens new report viewer form based on selected values
        private void getReportOnClick(object sender, EventArgs e)
        {
            try
            {
                if (viewReport == null)
                {
                    viewReport = new FormViewFurnitureReport(this);
                    viewReport.MdiParent = FormMainParent.Instance;
                    viewReport.FormClosed += new FormClosedEventHandler(FurnitureReport_FormClosed);
                    viewReport.StartPosition = FormStartPosition.CenterScreen;
                    this.Hide();
                    viewReport.Show();
                }
                else
                {
                    viewReport.Activate();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        //Sets open report form to null and shows this form with selected values reset
        private void FurnitureReport_FormClosed(object sender, FormClosedEventArgs e)
        {
            viewReport.Dispose();
            viewReport = null;
            reset();
            this.Show();
        }

        //Helper to reset combo box and dates to default values
        private void reset()
        {
            categoryBox.SelectedIndex = 0;
            startDatePicker.Value = rentalController.getMinDate();
            endDatePicker.Value = rentalController.getMinDate();
        }
    }
}
