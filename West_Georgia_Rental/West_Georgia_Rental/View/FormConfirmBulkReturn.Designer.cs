﻿namespace West_Georgia_Rental.View
{
    partial class FormConfirmBulkReturn
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnConfirmBulkReturn = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btnConfirmBulkReturn
            // 
            this.btnConfirmBulkReturn.AutoSize = true;
            this.btnConfirmBulkReturn.Location = new System.Drawing.Point(574, 12);
            this.btnConfirmBulkReturn.Name = "btnConfirmBulkReturn";
            this.btnConfirmBulkReturn.Size = new System.Drawing.Size(111, 23);
            this.btnConfirmBulkReturn.TabIndex = 0;
            this.btnConfirmBulkReturn.Text = "Confirm Bulk Return";
            this.btnConfirmBulkReturn.UseVisualStyleBackColor = true;
            this.btnConfirmBulkReturn.Click += new System.EventHandler(this.btnConfirmBulkReturn_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(713, 12);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 1;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // FormConfirmBulkReturn
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnConfirmBulkReturn);
            this.Name = "FormConfirmBulkReturn";
            this.Text = "Bulk Return Confirmation";
            this.Load += new System.EventHandler(this.FormConfirmBulkReturn_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnConfirmBulkReturn;
        private System.Windows.Forms.Button btnCancel;
    }
}