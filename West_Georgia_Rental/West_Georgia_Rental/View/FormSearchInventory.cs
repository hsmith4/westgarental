﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using West_Georgia_Rental.Controller;
using West_Georgia_Rental.Model;

namespace West_Georgia_Rental.View
{
    public partial class FormSearchInventory : Form
    {
        private static FormSearchInventory instance;
        private FormMemberManagement mmForm;
        private FormNewRentalForSelectedMember frmNewRental;
        private FormEmployeeMainMenu empMenu;

        private FurnitureController furnitureController;
        private FurnitureItemController itemController;
        private CategoryController categoryController;
        private StyleController styleController;
        private CartController cartController;

        private List<FurnitureListing> furnitureListing;
        private FurnitureListing item;
        private int availability;

    /***** FORM SETUP *****/
        
        //Default constructor
        public FormSearchInventory()
        {
            instance = this;
            InitializeComponent();
            furnitureController = new FurnitureController();
            itemController = new FurnitureItemController();
            categoryController = new CategoryController();
            styleController = new StyleController();
            furnitureListing = new List<FurnitureListing>();
            cartController = new CartController();
            //availability = "";
            label2.Text = "Enter a number between " + itemController.getMinSerial() + " and "
                + itemController.getMaxSerial() + " to search by Serial Number or enter '0' to see all listings ";
            label7.Text = "Enter a number between " + furnitureController.getMinID() + " and "
                + furnitureController.getMaxID() + " to search by ID Number or enter '0' to see all listings ";
        }

        //1-param constructor
        //Does everything the default constructor does
        //Also initializes the member management form with currently selected member
        public FormSearchInventory(FormMemberManagement mmForm) : this()
        {
            this.mmForm = mmForm;
        }

        //Returns current instance of this form
        public static FormSearchInventory Instance
        {
            get
            {
                return instance;
            }
        }

        //Fills combo boxes when form is loaded
        private void FormSearchInventory_Load(object sender, EventArgs e)
        {
            try
            {
                populateCategoryComboBox();
                populateStyleComboBox();
                populateDescriptionComboBox();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.GetType().ToString());
            }
        }
        //Helper to populate the category search combo box
        private void populateCategoryComboBox()
        {
            List<FurnitureListing> allCategories = categoryController.getAllCategories();

            try
            {
                FurnitureListing aListing;

                for (int current = 0; current < allCategories.Count; current++)
                {
                    aListing = allCategories[current];
                    categoryComboBox.Items.Add(aListing.category);
                }
            }
            catch (Exception)
            {
                MessageBox.Show("Category box could not be populated");
            }
        }
        //Helper to populate the style search combo box
        private void populateStyleComboBox()
        {
            List<FurnitureListing> allStyles = styleController.getAllStyles();

            try
            {
                FurnitureListing aListing;

                for (int current = 0; current < allStyles.Count; current++)
                {
                    aListing = allStyles[current];
                    styleComboBox.Items.Add(aListing.style);
                }
            }
            catch (Exception)
            {
                MessageBox.Show("Style box could not be populated");
            }
        }
        //Helper to populate the description search combo box
        private void populateDescriptionComboBox()
        {
            List<FurnitureListing> allDescription = furnitureController.getDescription();

            try
            {
                FurnitureListing aListing;

                for (int current = 0; current < allDescription.Count; current++)
                {
                    aListing = allDescription[current];
                    descriptionComboBox.Items.Add(aListing.description);
                }
            }
            catch (Exception)
            {
                MessageBox.Show("Description box could not be populated");
            }
        }

        //Formats 'Available' cell to show the availability of the item in the grid view
        private void DataGridView1_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            if (dataGridView1.Columns[e.ColumnIndex].Name == "Availability")
            {
                if (!itemController.isItemAvailable(Convert.ToInt32(dataGridView1.Rows[e.RowIndex].Cells[1].Value)))
                    e.Value = "No"; 
                else if (!cartController.isItemInCart(Convert.ToInt32(dataGridView1.Rows[e.RowIndex].Cells[1].Value)))
                {
                    e.Value = "No";
                }
                else
                    e.Value = "Yes";
            }
        }

    /***** BUTTON HANDLERS *****/

        private void backOnClick(object sender, EventArgs e)
        {
            this.Close();
            if (mmForm == null)
            {
                this.empMenu = new FormEmployeeMainMenu();
            }
        }

        //Adds the item to the selected member's cart
        //If a member is not selected then opens the member management page to select a member
        private void btnRentItem_Click(object sender, EventArgs e)
        {
            if (!cartController.isItemInCart(this.item.serialNumber))
            {
                MessageBox.Show("Item already in a cart. Please select a different item");
            }
            else if (dataGridView1.SelectedRows.Count != 1)
            {
                MessageBox.Show("Please select a single item.");
            }
            else if (!itemController.isItemAvailable(this.item.serialNumber))
            {
                MessageBox.Show("This item is not available to rent.  Please choose a different item.");
            }
            else if (mmForm == null)
            {
                try
                {
                    mmForm = new FormMemberManagement(this);
                    mmForm.MdiParent = FormMainParent.Instance;
                    mmForm.FormClosed += FrmNewRental_FormClosed;
                    mmForm.StartPosition = FormStartPosition.CenterScreen;
                    //this.Hide();
                    mmForm.Show();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, ex.GetType().ToString());
                }
                return;
            }
            else if (frmNewRental == null)
            {
                try
                {
                    frmNewRental = new FormNewRentalForSelectedMember(mmForm, this);
                    frmNewRental.MdiParent = FormMainParent.Instance;
                    frmNewRental.FormClosed += FrmNewRental_FormClosed;
                    frmNewRental.StartPosition = FormStartPosition.CenterScreen;
                    //this.Hide();
                    frmNewRental.Show();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, ex.GetType().ToString());
                }
            }
            else
            {
                frmNewRental.Activate();
            }
        }

        //Set new rental to null and shows this form when new rental form is closed
        private void FrmNewRental_FormClosed(object sender, FormClosedEventArgs e)
        {
            frmNewRental = null;
            this.Show();
        }

    /***** SEARCH FUNCTIONALITY *****/

        //Search by serial number
        private void searchSerialOnClick(object sender, EventArgs e)
        {
            try
            {
                int serialNumber = Int32.Parse(serialTextBox.Text);

                if (serialNumber == 0)
                {
                    furnitureListing = furnitureController.allListings();
                    dataGridView1.DataSource = furnitureListing;
                    dataGridView1.CellFormatting += DataGridView1_CellFormatting;
                }
                else if (serialNumber < itemController.getMinSerial() || serialNumber > itemController.getMaxSerial() && serialNumber != 0)
                {
                    MessageBox.Show("Serial number does not exist. Enter a number between " + itemController.getMinSerial() + " and "
                        + itemController.getMaxSerial());
                    return;
                }
                else
                {
                    furnitureListing = furnitureController.theListing(serialNumber);
                    dataGridView1.DataSource = furnitureListing;
                    dataGridView1.CellFormatting += DataGridView1_CellFormatting;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        //Search by category, style, and description
        //Or search by one of the above
        private void comboListingOnClick(object sender, EventArgs e)
        {
            string category = categoryComboBox.GetItemText(categoryComboBox.SelectedItem);
            string style = styleComboBox.GetItemText(styleComboBox.SelectedItem);
            string description = descriptionComboBox.GetItemText(descriptionComboBox.SelectedItem);

            if (category.Equals("") && style.Equals("") && description.Equals(""))
            {
                MessageBox.Show("Please select at least one attribute");
                return;
            }
            else if (style.Equals("") && description.Equals(""))
            {
                furnitureListing = furnitureController.theListingsByCategory(category);
                dataGridView1.DataSource = furnitureListing;
                dataGridView1.CellFormatting += DataGridView1_CellFormatting;
            }
            else if (category.Equals("") && description.Equals(""))
            {
                furnitureListing = furnitureController.theListingsByStyle(style);
                dataGridView1.DataSource = furnitureListing;
                dataGridView1.CellFormatting += DataGridView1_CellFormatting;
            }
            else if (category.Equals("") && style.Equals(""))
            {
                furnitureListing = furnitureController.theListingsByDescription(description);
                dataGridView1.DataSource = furnitureListing;
                dataGridView1.CellFormatting += DataGridView1_CellFormatting;
            }
            else if (category.Equals(""))
            {
                furnitureListing = furnitureController.theListingsByStyleAndDescription(style, description);
                dataGridView1.DataSource = furnitureListing;
                dataGridView1.CellFormatting += DataGridView1_CellFormatting;
            }
            else if (style.Equals(""))
            {
                furnitureListing = furnitureController.theListingsByCategoryAndDescription(category, description);
                dataGridView1.DataSource = furnitureListing;
                dataGridView1.CellFormatting += DataGridView1_CellFormatting;
            }
            else if (description.Equals(""))
            {
                furnitureListing = furnitureController.theListingsByCategoryAndStyle(category, style);
                dataGridView1.DataSource = furnitureListing;
                dataGridView1.CellFormatting += DataGridView1_CellFormatting;
            }
            else
            {
                furnitureListing = furnitureController.theListingsByAllAttributes(category, style, description);
                dataGridView1.DataSource = furnitureListing;
                dataGridView1.CellFormatting += DataGridView1_CellFormatting;
            }
           
            if (furnitureListing.Count == 0)
            {
                MessageBox.Show("There are no results meeting that criteria");
            }
        }
        //Search by furniture ID
        private void getByIDOnClick(object sender, EventArgs e)
        {
            try
            {
                int id = Int32.Parse(idTextBox.Text);

                if (id == 0)
                {
                    furnitureListing = furnitureController.allListings();
                    dataGridView1.DataSource = furnitureListing;
                    dataGridView1.CellFormatting += DataGridView1_CellFormatting;
                }
                else if (id < furnitureController.getMinID() || id > furnitureController.getMaxID() && id != 0)
                {
                    MessageBox.Show("ID number does not exist. Enter a number between " + furnitureController.getMinID() + " and "
                        + furnitureController.getMaxID());
                    return;
                }
                else
                {
                    furnitureListing = furnitureController.theListing(id);
                    dataGridView1.DataSource = furnitureListing;
                    dataGridView1.CellFormatting += DataGridView1_CellFormatting;
                }
            }
            catch (Exception)
            {
                MessageBox.Show("Please enter a valid number");
            }
        }
        //Clear the search fields and data grid view
        private void clearOnClick(object sender, EventArgs e)
        {
            serialTextBox.Text = "";
            idTextBox.Text = "";
            categoryComboBox.SelectedItem = null;
            styleComboBox.SelectedItem = null;
            descriptionComboBox.SelectedItem = null;
            furnitureListing = null;
            dataGridView1.DataSource = furnitureListing;
        }

    /***** GET SELECTED ITEM *****/

        private void dataGridView1_SelectionChanged(object sender, EventArgs e)
        {
            if (dataGridView1.SelectedRows.Count == 1)
            {
                int serialNumber = Convert.ToInt32(dataGridView1.SelectedRows[0].Cells[1].Value);
                if (serialNumber > 0)
                {
                    this.item = this.furnitureController.getFurnitureBySerialNumber(serialNumber);
                    this.availability = itemController.getItemAvailability(serialNumber);
                }
            }
        }

        //Returns the current item
        public FurnitureListing getItem()
        {
            return this.item;
        }

        //Setter to set member management form to passed form
        public void setMemberManagementForm(FormMemberManagement mmForm)
        {
            this.mmForm = mmForm;
        }
    }
}
