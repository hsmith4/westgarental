﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using West_Georgia_Rental.Model;

namespace West_Georgia_Rental.View
{
    public partial class FormEmployeeMainMenu : Form
    {
        private string name;
        private int isAdmin;
        private static FormEmployeeMainMenu instance;
        private static FormSearchInventory searchInventory;
        private static FormMemberManagement frmMemberManagement;
        private static FormUpdatePassword updatePassword;
        private static FormReportParameters reportParameters;

        //Initializes the form. Report button is disabled if user is not an admin
        public FormEmployeeMainMenu()
        {
            instance = this;
            searchInventory = null;
            updatePassword = null;
            InitializeComponent();
            setLabel();

            if (!isUserAdmin(isAdmin))
            {
                reportButton.Enabled = false;
            }
        }

        //Sets the labels to defined values
        public void setLabel()
        {
            //if (CurrentUser.User )
            name = CurrentUser.User.username;
            isAdmin = CurrentUser.User.inAdmin;
            label1.Text = "Welcome " + name + ". Please select one of the options from below:";
        }

        //Returns true if the current user is an admin
        private bool isUserAdmin(int value)
        {
            bool admin = true;

            if (value == 0)
            {
                admin = false;
            }
            return admin;
        }

        //Returns the user employee id
        public int empID()
        {
            return CurrentUser.User.empID;
        }

        //Returns current instance of this form
        public static FormEmployeeMainMenu Instance
        {
            get
            {
                return instance;
            }
        }

        //Creates new instance of search form if null when button clicked
        private void searchOnClick(object sender, EventArgs e)
        {
            if (searchInventory == null)
            {
                searchInventory = new FormSearchInventory();
                searchInventory.MdiParent = FormMainParent.Instance;
                searchInventory.FormClosed += new FormClosedEventHandler(SearchInventory_FormClosed);
                searchInventory.StartPosition = FormStartPosition.CenterScreen;
                this.Hide();
                searchInventory.Show();
            }
            else
            {
                searchInventory.Activate();
            }
        }

        //Sets search form to null on close and shows this form
        private void SearchInventory_FormClosed(object sender, FormClosedEventArgs e)
        {
            searchInventory.Dispose();
            searchInventory = null;
            this.Show();
        }

        //Creates new instance of member management form if null and hides this form
        private void memberButton_Click(object sender, EventArgs e)
        {
            if (frmMemberManagement == null)
            {
                frmMemberManagement = new FormMemberManagement();
                frmMemberManagement.MdiParent = FormMainParent.Instance;
                frmMemberManagement.FormClosed += new FormClosedEventHandler(FrmMemberManagement_FormClosed);
                frmMemberManagement.StartPosition = FormStartPosition.CenterScreen;
                this.Hide();
                frmMemberManagement.Show();
            }
            else
            {
                frmMemberManagement.Activate();
            }
        }

        //Sets member management form to null on close
        private void FrmMemberManagement_FormClosed(object sender, FormClosedEventArgs e)
        {
            frmMemberManagement.Dispose();
            frmMemberManagement = null;
            if (frmMemberManagement == null && searchInventory == null)
                this.Show();
        }

        //Creates new instance of password form if null. Hides this form
        private void changePasswordOnClick(object sender, EventArgs e)
        {
            if (updatePassword == null)
            {
                updatePassword = new FormUpdatePassword(name);
                updatePassword.MdiParent = FormMainParent.Instance;
                updatePassword.FormClosed += new FormClosedEventHandler(UpdatePassword_FormClosed);
                updatePassword.StartPosition = FormStartPosition.CenterScreen;
                this.Hide();
                updatePassword.Show();
            }
            else
            {
                updatePassword.Activate();
            }
        }

        //Sets password form to null on close
        private void UpdatePassword_FormClosed(object sender, FormClosedEventArgs e)
        {
            updatePassword.Dispose();
            updatePassword = null;
            this.Show();
        }

        //Creates report form instance if null
        private void getReportOnClick(object sender, EventArgs e)
        {
            try
            {
                if (reportParameters == null)
                {
                    reportParameters = new FormReportParameters();
                    reportParameters.MdiParent = FormMainParent.Instance;
                    reportParameters.FormClosed += new FormClosedEventHandler(ReportParameter_FormClosed);
                    reportParameters.StartPosition = FormStartPosition.CenterScreen;
                    reportParameters.Show();
                }
                else
                {
                    reportParameters.Activate();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        //Sets report form to null on close
        private void ReportParameter_FormClosed(object sender, FormClosedEventArgs e)
        {
            reportParameters.Dispose();
            reportParameters = null;
        }
    }
}
