﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using West_Georgia_Rental.Model;

namespace West_Georgia_Rental.DAL
{
    public static class CartDAL
    {
        //Creates new cart listing with passed attributes
        public static int addToCart(int memberID, int serialNumber, int empID, int length)
        {
            int addedToCart = 0;
            SqlConnection connection = RentalDBA.GetConnection();
            string insertStatement =
                "INSERT cart " +
                "(memberID, serialNumber, empID, lengthOfRental) " +
                "VALUES " +
                "(@MemberID, @SerialNumber, @EmployeeID, @LengthOfRental)";
            SqlCommand insertCommand = new SqlCommand(insertStatement, connection);
            insertCommand.Parameters.AddWithValue("@MemberID", memberID);
            insertCommand.Parameters.AddWithValue("@SerialNumber", serialNumber);
            insertCommand.Parameters.AddWithValue("@EmployeeID", empID);
            insertCommand.Parameters.AddWithValue("@LengthOfRental", length);

            try
            {
                connection.Open();
                addedToCart = insertCommand.ExecuteNonQuery();
            }
            catch (SqlException)
            {
                throw;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                if (connection != null)
                    connection.Close();
            }
            return addedToCart;
        }

        //Gets return date for item with member id
        public static DateTime getItemReturnDate(int memberID, int serialNumber)
        {
            DateTime returnDate;
            SqlConnection connection = RentalDBA.GetConnection();
            string selectStatement = "select dateAdd(day, lengthOfRental, getdate()) as 'returnDate' " +
                                     "FROM cart " +
                                     "where memberID = @MemberID " +
                                     "and serialNumber = @SerialNumber";
            SqlCommand selectCommand = new SqlCommand(selectStatement, connection);
            selectCommand.Parameters.AddWithValue("@MemberID", memberID);
            selectCommand.Parameters.AddWithValue("@SerialNumber", serialNumber);
            SqlDataReader reader = null;
            try
            {
                connection.Open();
                returnDate = (DateTime)selectCommand.ExecuteScalar();

            }
            catch (SqlException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (connection != null)
                    connection.Close();
                if (reader != null)
                    reader.Close();
            }

            return returnDate;
        }

        //Returns cart list by member id
        public static List<Cart> getCartByMemberID(int memberID)
        {
            List<Cart> cart = new List<Cart>();
            SqlConnection connection = RentalDBA.GetConnection();
            string selectStatement = "SELECT m.fName, m.lName, f.furnitureID, c.serialNumber, f.description, cat.categoryName, s.StyleName " +  
                                    "FROM cart c inner join member m " +
                                    "on c.memberID = m.memberID " +
                                    "inner join furnitureItem fi " +
                                    "on fi.serialNumber = c.serialNumber " +
                                    "inner join furniture f " +
                                    "on f.furnitureID = fi.furnitureID " +
                                    "inner join style s " +
                                    "on s.styleID = f.styleID " +
                                    "inner join category cat " +
                                    "on cat.categoryID = f.categoryID " +
                                    "where c.memberID = @MemberID";
            SqlCommand selectCommand = new SqlCommand(selectStatement, connection);
            selectCommand.Parameters.AddWithValue("@MemberID", memberID);
            SqlDataReader reader = null;
            try
            {
                connection.Open();
                reader = selectCommand.ExecuteReader();
                while (reader.Read())
                {
                    Cart memberCart = new Cart();
                    memberCart.serialNumber = (int)reader["serialNumber"];
                    memberCart.furnitureID = (int)reader["furnitureID"];
                    memberCart.fName = reader["fName"].ToString();
                    memberCart.lName = reader["lName"].ToString();
                    memberCart.categoryName = reader["categoryName"].ToString();
                    memberCart.description = reader["description"].ToString();
                    memberCart.styleName = reader["styleName"].ToString();
                    //memberCart.returnDate = (DateTime)reader["returnDate"];
                    cart.Add(memberCart);
                }
            }
            catch (SqlException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (connection != null)
                    connection.Close();
                if (reader != null)
                    reader.Close();
            }
            return cart;
        }

        //Returns list of all carts
        public static List<Cart> getAllCarts()
        {
            List<Cart> cart = new List<Cart>();
            SqlConnection connection = RentalDBA.GetConnection();
            string selectStatement = "SELECT serialNumber FROM cart";
            SqlCommand selectCommand = new SqlCommand(selectStatement, connection);
            SqlDataReader reader = null;
            try
            {
                connection.Open();
                reader = selectCommand.ExecuteReader();
                while (reader.Read())
                {
                    Cart memberCart = new Cart();
                    memberCart.serialNumber = (int)reader["serialNumber"];
                   
                    cart.Add(memberCart);
                }
            }
            catch (SqlException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (connection != null)
                    connection.Close();
                if (reader != null)
                    reader.Close();
            }
            return cart;
        }

        //Counts all items in cart for passed member
        public static int countItemsInMemberCart(int memberID)
        {
            int count = 0;
            SqlConnection connection = RentalDBA.GetConnection();
            string selectStatement = "SELECT count(*) as 'count' " +
                                    "FROM cart " +
                                    "where memberID = @MemberID";
            SqlCommand selectCommand = new SqlCommand(selectStatement, connection);
            selectCommand.Parameters.AddWithValue("@MemberID", memberID);
            SqlDataReader reader = null;
            try
            {
                connection.Open();
                reader = selectCommand.ExecuteReader(System.Data.CommandBehavior.SingleRow);
                if (reader.Read())
                {
                    count = Convert.ToInt32(reader["count"]);
                }
            }
            catch (SqlException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (connection != null)
                    connection.Close();
                if (reader != null)
                    reader.Close();
            }
            return count;
        }

        //Deletes passed item from passed member's cart
        public static int deleteCartItem(int memberID, int serialNumber)
        {
            int rowCount = 0;
            SqlConnection connection = RentalDBA.GetConnection();
            string deleteStatement = "DELETE FROM cart " +
                "WHERE memberID = @MemberID " +
                "AND serialNumber = @SerialNumber";
            SqlCommand deleteCommand = new SqlCommand(deleteStatement, connection);
            deleteCommand.Parameters.AddWithValue("@MemberID", memberID);
            deleteCommand.Parameters.AddWithValue("@SerialNumber", serialNumber);
            SqlDataReader reader = null;
            try
            {
                connection.Open();
                rowCount = deleteCommand.ExecuteNonQuery();
            }
            catch (SqlException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (connection != null)
                    connection.Close();
                if (reader != null)
                    reader.Close();
            }
            return rowCount;
        }

        //Returns cart price total
        public static decimal getCartTotalForMember(int memberID)
        {
            decimal total;

            SqlConnection connection = RentalDBA.GetConnection();
            string selectStatement = "SELECT SUM(c.lengthOfRental * f.dailyRentalRate) " +
                "FROM cart c INNER JOIN furnitureItem fi " +
                "ON c.serialNumber = fi.serialNumber " +
                "INNER JOIN furniture f " +
                "ON fi.furnitureID = f.furnitureID " +
                "WHERE memberID = @MemberID";
            SqlCommand selectCommand = new SqlCommand(selectStatement, connection);
            selectCommand.Parameters.AddWithValue("@MemberID", memberID);
            SqlDataReader reader = null;
            try
            {
                connection.Open();
                total = Convert.ToDecimal(selectCommand.ExecuteScalar());
                
            }
            catch (SqlException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (connection != null)
                    connection.Close();
                if (reader != null)
                    reader.Close();
            }

            return total;

        }

        //Gets price for passed item
        public static decimal getItemTotal(int serialNumber)
        {
            decimal total;

            SqlConnection connection = RentalDBA.GetConnection();
            string selectStatement = "SELECT (c.lengthOfRental * f.dailyRentalRate) " +
                "FROM cart c INNER JOIN furnitureItem fi " +
                "ON c.serialNumber = fi.serialNumber " +
                "INNER JOIN furniture f " +
                "ON fi.furnitureID = f.furnitureID " +
                "WHERE c.serialNumber = @SerialNumber";
            SqlCommand selectCommand = new SqlCommand(selectStatement, connection);
            selectCommand.Parameters.AddWithValue("@SerialNumber", serialNumber);
            SqlDataReader reader = null;
            try
            {
                connection.Open();
                total = Convert.ToDecimal(selectCommand.ExecuteScalar());
            }
            catch (SqlException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (connection != null)
                    connection.Close();
                if (reader != null)
                    reader.Close();
            }

            return total;
        }

        //Returns passed item cart
        public static Cart getCartItemBySerialNumber(int serialNumber)
        {
            Cart cart = new Cart();
            SqlConnection connection = RentalDBA.GetConnection();
            string selectStatement = "SELECT c.serialNumber, c.lengthOfRental, f.description, cat.categoryName, s.StyleName, f.dailyRentalRate, dateAdd(day, c.lengthOfRental, getdate()) as 'returnDate' " +  
                                    "FROM cart c inner join member m " +
                                    "on c.memberID = m.memberID " +
                                    "inner join furnitureItem fi " +
                                    "on fi.serialNumber = c.serialNumber " +
                                    "inner join furniture f " +
                                    "on f.furnitureID = fi.furnitureID " +
                                    "inner join style s " +
                                    "on s.styleID = f.styleID " +
                                    "inner join category cat " +
                                    "on cat.categoryID = f.categoryID " +
                                    "WHERE c.serialNumber = @SerialNumber";
            SqlCommand selectCommand = new SqlCommand(selectStatement, connection);
            selectCommand.Parameters.AddWithValue("@SerialNumber", serialNumber);
            SqlDataReader reader = null;
            try
            {
                connection.Open();
                
                reader = selectCommand.ExecuteReader(System.Data.CommandBehavior.SingleRow);
                int dailyRate = reader.GetOrdinal("dailyRentalRate");
                if (reader.Read())
                {
                    cart.serialNumber = (int)reader["serialNumber"];
                    cart.categoryName = reader["categoryName"].ToString();
                    cart.description = reader["description"].ToString();
                    cart.styleName = reader["styleName"].ToString();
                    cart.returnDate = (DateTime)reader["returnDate"];
                    cart.dailyRate = string.Format("${0:0.00}", (double)reader.GetDecimal(dailyRate));
                    cart.lengthOfRental = Convert.ToInt32(reader["lengthOfRental"]);
                }
            }
            catch (SqlException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (connection != null)
                    connection.Close();
                if (reader != null)
                    reader.Close();
            }
            return cart;
        }

        //Updates rental length for passed item
        public static int updateCartItem(int serialNumber, int length)
        {
            int rowsUpdated = 0;

            SqlConnection connection = RentalDBA.GetConnection();
            string updateStatement = "UPDATE cart " +
                "SET lengthOfRental = @NewLength " +
                "WHERE serialNumber = @SerialNumber";
            SqlCommand updateCommand = new SqlCommand(updateStatement, connection);
            updateCommand.Parameters.AddWithValue("@NewLength", length);
            updateCommand.Parameters.AddWithValue("@SerialNumber", serialNumber);
            SqlDataReader reader = null;

            try
            {
                connection.Open();
                rowsUpdated = updateCommand.ExecuteNonQuery();
            }
            catch (SqlException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (connection != null)
                    connection.Close();
                if (reader != null)
                    reader.Close();
            }

            return rowsUpdated;
        }

        //Returns true if passed item is in cart
        public static bool isItemInCart(int serialNumber)
        {
            string theItem;
            bool available = false;
            SqlConnection connection = RentalDBA.GetConnection();
            string selectStatement = "SELECT serialNumber " +
                "FROM cart " +
                "WHERE serialNumber = @SerialNumber";
            SqlCommand selectCommand = new SqlCommand(selectStatement, connection);
            selectCommand.Parameters.AddWithValue("@SerialNumber", serialNumber);
            SqlDataReader reader = null;
            try
            {
                connection.Open();
                theItem = Convert.ToString(selectCommand.ExecuteScalar());

                if (theItem == "")
                {
                    available = true;
                }
            }
            catch (SqlException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (connection != null)
                    connection.Close();
                if (reader != null)
                    reader.Close();
            }
            return available;
        }
    }
}
