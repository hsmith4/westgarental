﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace West_Georgia_Rental.DAL
{
    class RentalDAL
    {
        //Creates a new rental based on the passed member and employee
        public static int createRental(int memberID, int empID)
        {
            int rentalID = 0;
            SqlConnection connection = RentalDBA.GetConnection();
            string insertStatement =
                "INSERT into rental " +
                "(memberID, empID, rentalDate) " +
                "VALUES (@MemberID, @EmployeeID, getdate())";
            SqlCommand insertCommand = new SqlCommand(insertStatement, connection);
            insertCommand.Parameters.AddWithValue("@MemberID", memberID);
            insertCommand.Parameters.AddWithValue("@EmployeeID", empID);

            try
            {
                connection.Open();
                insertCommand.ExecuteNonQuery();
                string selectStatement =
                    "SELECT IDENT_CURRENT('rental') FROM rental";
                SqlCommand selectCommand = new SqlCommand(selectStatement, connection);
                rentalID = Convert.ToInt32(selectCommand.ExecuteScalar());

            }
            catch (SqlException)
            {
                throw;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                if (connection != null)
                    connection.Close();
            }
            return rentalID;
        }

        //Returns the earliest date in the rental table
        public static DateTime getMinDate()
        {
            DateTime date;

            SqlConnection connection = RentalDBA.GetConnection();
            string selectStatement =
                "SELECT MIN(rentalDate) FROM rental ";

            SqlCommand selectCommand = new SqlCommand(selectStatement, connection);

            try
            {
                connection.Open();
                selectCommand.ExecuteNonQuery();
                date = (DateTime)selectCommand.ExecuteScalar();
            }
            catch (SqlException ex)
            {
                throw ex;
            }
            finally
            {
                connection.Close();
            }
            return date;
        }
    }
}
