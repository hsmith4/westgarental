﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using West_Georgia_Rental.Model;

namespace West_Georgia_Rental.DAL
{
    public static class FurnitureDAL
    {
        //Returns a list of furnitiure with passed serial number
        public static List<FurnitureListing> theListing(int serialNumber)
        {
            List<FurnitureListing> listing = new List<FurnitureListing>();

            SqlConnection connection = RentalDBA.GetConnection();
            string selectStatement =
                "SELECT serialNumber, c.categoryName, s.styleName, " +
                "f.description, f.dailyRentalRate, f.fineRate " +
                "FROM furniture f " +
                "JOIN furnitureItem i ON f.furnitureID = i.furnitureID " +
                "JOIN category c ON f.categoryID = c.categoryID " +
                "JOIN style s ON f.styleID = s.styleID " +
                "WHERE i.serialNumber = @serialNumber";// OR f.furnitureID = @serialNumber";

            SqlCommand selectCommand = new SqlCommand(selectStatement, connection);
            selectCommand.Parameters.AddWithValue("@serialNumber", serialNumber);

            try
            {
                connection.Open();
                SqlDataReader reader = selectCommand.ExecuteReader();
                int serial = reader.GetOrdinal("serialNumber");
                int rate = reader.GetOrdinal("dailyRentalRate");
                int fine = reader.GetOrdinal("fineRate");

                while (reader.Read())
                {
                    FurnitureListing aListing = new FurnitureListing();
                    aListing.serialNumber = reader.GetInt32(serial);
                    aListing.category = reader["categoryName"].ToString();
                    aListing.style = reader["styleName"].ToString();
                    aListing.description = reader["description"].ToString();
                    aListing.dailyRate = string.Format("${0:0.00}", (double)reader.GetDecimal(rate));
                    aListing.dailyFine = string.Format("${0:0.00}", (double)reader.GetDecimal(fine));

                    listing.Add(aListing);
                }
                reader.Close();
            }
            catch (SqlException ex)
            {
                throw ex;
            }
            finally
            {
                connection.Close();
            }
            return listing;
        }

        //Returns list of furniture of passed category
        public static List<FurnitureListing> theListingsByCategory(string category)
        {
            List<FurnitureListing> listing = new List<FurnitureListing>();

            SqlConnection connection = RentalDBA.GetConnection();
            string selectStatement =
                "SELECT serialNumber, c.categoryName, s.styleName, " +
                "f.description, f.dailyRentalRate, f.fineRate " +
                "FROM furniture f " +
                "JOIN furnitureItem i ON f.furnitureID = i.furnitureID " +
                "JOIN category c ON f.categoryID = c.categoryID " +
                "JOIN style s ON f.styleID = s.styleID " +
                "WHERE c.categoryName = @category";

            SqlCommand selectCommand = new SqlCommand(selectStatement, connection);
            selectCommand.Parameters.AddWithValue("@category", category);

            try
            {
                connection.Open();
                SqlDataReader reader = selectCommand.ExecuteReader();
                int serial = reader.GetOrdinal("serialNumber");
                int rate = reader.GetOrdinal("dailyRentalRate");
                int fine = reader.GetOrdinal("fineRate");

                while (reader.Read())
                {
                    FurnitureListing aListing = new FurnitureListing();
                    aListing.serialNumber = reader.GetInt32(serial);
                    aListing.category = reader["categoryName"].ToString();
                    aListing.style = reader["styleName"].ToString();
                    aListing.description = reader["description"].ToString();
                    aListing.dailyRate = string.Format("${0:0.00}", (double)reader.GetDecimal(rate));
                    aListing.dailyFine = string.Format("${0:0.00}", (double)reader.GetDecimal(fine));

                    listing.Add(aListing);
                }
                reader.Close();
            }
            catch (SqlException ex)
            {
                throw ex;
            }
            finally
            {
                connection.Close();
            }
            return listing;
        }

        //Returns list of furniture with passed style
        public static List<FurnitureListing> theListingsByStyle(string style)
        {
            List<FurnitureListing> listing = new List<FurnitureListing>();

            SqlConnection connection = RentalDBA.GetConnection();
            string selectStatement =
                "SELECT serialNumber, c.categoryName, s.styleName, " +
                "f.description, f.dailyRentalRate, f.fineRate " +
                "FROM furniture f " +
                "JOIN furnitureItem i ON f.furnitureID = i.furnitureID " +
                "JOIN category c ON f.categoryID = c.categoryID " +
                "JOIN style s ON f.styleID = s.styleID " +
                "WHERE s.styleName = @style";

            SqlCommand selectCommand = new SqlCommand(selectStatement, connection);
            selectCommand.Parameters.AddWithValue("@style", style);

            try
            {
                connection.Open();
                SqlDataReader reader = selectCommand.ExecuteReader();
                int serial = reader.GetOrdinal("serialNumber");
                int rate = reader.GetOrdinal("dailyRentalRate");
                int fine = reader.GetOrdinal("fineRate");

                while (reader.Read())
                {
                    FurnitureListing aListing = new FurnitureListing();
                    aListing.serialNumber = reader.GetInt32(serial);
                    aListing.category = reader["categoryName"].ToString();
                    aListing.style = reader["styleName"].ToString();
                    aListing.description = reader["description"].ToString();
                    aListing.dailyRate = string.Format("${0:0.00}", (double)reader.GetDecimal(rate));
                    aListing.dailyFine = string.Format("${0:0.00}", (double)reader.GetDecimal(fine));

                    listing.Add(aListing);
                }
                reader.Close();
            }
            catch (SqlException ex)
            {
                throw ex;
            }
            finally
            {
                connection.Close();
            }
            return listing;
        }

        //Returns list of furniture with passed description
        public static List<FurnitureListing> theListingsByDescription(string description)
        {
            List<FurnitureListing> listing = new List<FurnitureListing>();

            SqlConnection connection = RentalDBA.GetConnection();
            string selectStatement =
                "SELECT serialNumber, c.categoryName, s.styleName, " +
                "f.description, f.dailyRentalRate, f.fineRate " +
                "FROM furniture f " +
                "JOIN furnitureItem i ON f.furnitureID = i.furnitureID " +
                "JOIN category c ON f.categoryID = c.categoryID " +
                "JOIN style s ON f.styleID = s.styleID " +
                "WHERE f.description = @description";

            SqlCommand selectCommand = new SqlCommand(selectStatement, connection);
            selectCommand.Parameters.AddWithValue("@description", description);

            try
            {
                connection.Open();
                SqlDataReader reader = selectCommand.ExecuteReader();
                int serial = reader.GetOrdinal("serialNumber");
                int rate = reader.GetOrdinal("dailyRentalRate");
                int fine = reader.GetOrdinal("fineRate");

                while (reader.Read())
                {
                    FurnitureListing aListing = new FurnitureListing();
                    aListing.serialNumber = reader.GetInt32(serial);
                    aListing.category = reader["categoryName"].ToString();
                    aListing.style = reader["styleName"].ToString();
                    aListing.description = reader["description"].ToString();
                    aListing.dailyRate = string.Format("${0:0.00}", (double)reader.GetDecimal(rate));
                    aListing.dailyFine = string.Format("${0:0.00}", (double)reader.GetDecimal(fine));

                    listing.Add(aListing);
                }
                reader.Close();
            }
            catch (SqlException ex)
            {
                throw ex;
            }
            finally
            {
                connection.Close();
            }
            return listing;
        }

        //Returns list of furniture with passed style and description
        public static List<FurnitureListing> theListingsByStyleAndDescription(string style, string description)
        {
            List<FurnitureListing> listing = new List<FurnitureListing>();

            SqlConnection connection = RentalDBA.GetConnection();
            string selectStatement =
                "SELECT serialNumber, c.categoryName, s.styleName, " +
                "f.description, f.dailyRentalRate, f.fineRate " +
                "FROM furniture f " +
                "JOIN furnitureItem i ON f.furnitureID = i.furnitureID " +
                "JOIN category c ON f.categoryID = c.categoryID " +
                "JOIN style s ON f.styleID = s.styleID " +
                "WHERE s.styleName = @style AND f.description = @description";

            SqlCommand selectCommand = new SqlCommand(selectStatement, connection);
            selectCommand.Parameters.AddWithValue("@style", style);
            selectCommand.Parameters.AddWithValue("@description", description);

            try
            {
                connection.Open();
                SqlDataReader reader = selectCommand.ExecuteReader();
                int serial = reader.GetOrdinal("serialNumber");
                int rate = reader.GetOrdinal("dailyRentalRate");
                int fine = reader.GetOrdinal("fineRate");

                while (reader.Read())
                {
                    FurnitureListing aListing = new FurnitureListing();
                    aListing.serialNumber = reader.GetInt32(serial);
                    aListing.category = reader["categoryName"].ToString();
                    aListing.style = reader["styleName"].ToString();
                    aListing.description = reader["description"].ToString();
                    aListing.dailyRate = string.Format("${0:0.00}", (double)reader.GetDecimal(rate));
                    aListing.dailyFine = string.Format("${0:0.00}", (double)reader.GetDecimal(fine));

                    listing.Add(aListing);
                }
                reader.Close();
            }
            catch (SqlException ex)
            {
                throw ex;
            }
            finally
            {
                connection.Close();
            }
            return listing;
        }

        //Returns list of furniture with passed category and description
        public static List<FurnitureListing> theListingsByCategoryAndDescription(string category, string description)
        {
            List<FurnitureListing> listing = new List<FurnitureListing>();

            SqlConnection connection = RentalDBA.GetConnection();
            string selectStatement =
                "SELECT serialNumber, c.categoryName, s.styleName, " +
                "f.description, f.dailyRentalRate, f.fineRate " +
                "FROM furniture f " +
                "JOIN furnitureItem i ON f.furnitureID = i.furnitureID " +
                "JOIN category c ON f.categoryID = c.categoryID " +
                "JOIN style s ON f.styleID = s.styleID " +
                "WHERE c.categoryName = @category AND f.description = @description";

            SqlCommand selectCommand = new SqlCommand(selectStatement, connection);
            selectCommand.Parameters.AddWithValue("@category", category);
            selectCommand.Parameters.AddWithValue("@description", description);

            try
            {
                connection.Open();
                SqlDataReader reader = selectCommand.ExecuteReader();
                int serial = reader.GetOrdinal("serialNumber");
                int rate = reader.GetOrdinal("dailyRentalRate");
                int fine = reader.GetOrdinal("fineRate");

                while (reader.Read())
                {
                    FurnitureListing aListing = new FurnitureListing();
                    aListing.serialNumber = reader.GetInt32(serial);
                    aListing.category = reader["categoryName"].ToString();
                    aListing.style = reader["styleName"].ToString();
                    aListing.description = reader["description"].ToString();
                    aListing.dailyRate = string.Format("${0:0.00}", (double)reader.GetDecimal(rate));
                    aListing.dailyFine = string.Format("${0:0.00}", (double)reader.GetDecimal(fine));

                    listing.Add(aListing);
                }
                reader.Close();
            }
            catch (SqlException ex)
            {
                throw ex;
            }
            finally
            {
                connection.Close();
            }
            return listing;
        }

        //Returns list of furniture with passed category and style
        public static List<FurnitureListing> theListingsByCategoryAndStyle(string category, string style)
        {
            List<FurnitureListing> listing = new List<FurnitureListing>();

            SqlConnection connection = RentalDBA.GetConnection();
            string selectStatement =
                "SELECT serialNumber, c.categoryName, s.styleName, " +
                "f.description, f.dailyRentalRate, f.fineRate " +
                "FROM furniture f " +
                "JOIN furnitureItem i ON f.furnitureID = i.furnitureID " +
                "JOIN category c ON f.categoryID = c.categoryID " +
                "JOIN style s ON f.styleID = s.styleID " +
                "WHERE c.categoryName = @category AND s.styleName = @style";

            SqlCommand selectCommand = new SqlCommand(selectStatement, connection);
            selectCommand.Parameters.AddWithValue("@category", category);
            selectCommand.Parameters.AddWithValue("@style", style);

            try
            {
                connection.Open();
                SqlDataReader reader = selectCommand.ExecuteReader();
                int serial = reader.GetOrdinal("serialNumber");
                int rate = reader.GetOrdinal("dailyRentalRate");
                int fine = reader.GetOrdinal("fineRate");

                while (reader.Read())
                {
                    FurnitureListing aListing = new FurnitureListing();
                    aListing.serialNumber = reader.GetInt32(serial);
                    aListing.category = reader["categoryName"].ToString();
                    aListing.style = reader["styleName"].ToString();
                    aListing.description = reader["description"].ToString();
                    aListing.dailyRate = string.Format("${0:0.00}", (double)reader.GetDecimal(rate));
                    aListing.dailyFine = string.Format("${0:0.00}", (double)reader.GetDecimal(fine));

                    listing.Add(aListing);
                }
                reader.Close();
            }
            catch (SqlException ex)
            {
                throw ex;
            }
            finally
            {
                connection.Close();
            }
            return listing;
        }

        //Returns list of furniture with all passed attributes
        public static List<FurnitureListing> theListingsByAllAttributes(string category, string style, string description)
        {
            List<FurnitureListing> listing = new List<FurnitureListing>();

            SqlConnection connection = RentalDBA.GetConnection();
            string selectStatement =
                "SELECT serialNumber, c.categoryName, s.styleName, " +
                "f.description, f.dailyRentalRate, f.fineRate " +
                "FROM furniture f " +
                "JOIN furnitureItem i ON f.furnitureID = i.furnitureID " +
                "JOIN category c ON f.categoryID = c.categoryID " +
                "JOIN style s ON f.styleID = s.styleID " +
                "WHERE c.categoryName = @category AND s.styleName = @style AND f.description = @description";

            SqlCommand selectCommand = new SqlCommand(selectStatement, connection);
            selectCommand.Parameters.AddWithValue("@category", category);
            selectCommand.Parameters.AddWithValue("@style", style);
            selectCommand.Parameters.AddWithValue("@description", description);

            try
            {
                connection.Open();
                SqlDataReader reader = selectCommand.ExecuteReader();
                int serial = reader.GetOrdinal("serialNumber");
                int rate = reader.GetOrdinal("dailyRentalRate");
                int fine = reader.GetOrdinal("fineRate");

                while (reader.Read())
                {
                    FurnitureListing aListing = new FurnitureListing();
                    aListing.serialNumber = reader.GetInt32(serial);
                    aListing.category = reader["categoryName"].ToString();
                    aListing.style = reader["styleName"].ToString();
                    aListing.description = reader["description"].ToString();
                    aListing.dailyRate = string.Format("${0:0.00}", (double)reader.GetDecimal(rate));
                    aListing.dailyFine = string.Format("${0:0.00}", (double)reader.GetDecimal(fine));

                    listing.Add(aListing);
                }
                reader.Close();
            }
            catch (SqlException ex)
            {
                throw ex;
            }
            finally
            {
                connection.Close();
            }
            return listing;
        }

        //Returns list of all furniture
        public static List<FurnitureListing> allListings()
        {
            List<FurnitureListing> listing = new List<FurnitureListing>();

            SqlConnection connection = RentalDBA.GetConnection();
            string selectStatement =
                "SELECT i.serialNumber, c.categoryName, s.styleName, " +
                "f.description, f.dailyRentalRate, f.fineRate " +
                "FROM furniture f " +
                "JOIN furnitureItem i ON f.furnitureID = i.furnitureID " +
                "JOIN category c ON f.categoryID = c.categoryID " +
                "JOIN style s ON f.styleID = s.styleID ";

            SqlCommand selectCommand = new SqlCommand(selectStatement, connection);
            try
            {
                connection.Open();
                SqlDataReader reader = selectCommand.ExecuteReader();
                int serial = reader.GetOrdinal("serialNumber");
                int rate = reader.GetOrdinal("dailyRentalRate");
                int fine = reader.GetOrdinal("fineRate");

                while (reader.Read())
                {
                    FurnitureListing aListing = new FurnitureListing();
                    aListing.serialNumber = reader.GetInt32(serial);
                    aListing.category = reader["categoryName"].ToString();
                    aListing.style = reader["styleName"].ToString();
                    aListing.description = reader["description"].ToString();
                    aListing.dailyRate = string.Format("${0:0.00}", (double)reader.GetDecimal(rate));
                    aListing.dailyFine = string.Format("${0:0.00}", (double)reader.GetDecimal(fine));

                    listing.Add(aListing);
                }
                reader.Close();
            }
            catch (SqlException ex)
            {
                throw ex;
            }
            finally
            {
                connection.Close();
            }
            return listing;
        }

        //Returns list of all furniture descriptions
        public static List<FurnitureListing> getDescription()
        {
            List<FurnitureListing> listing = new List<FurnitureListing>();

            SqlConnection connection = RentalDBA.GetConnection();
            string selectStatement =
                "SELECT description " +
                "FROM furniture ";

            SqlCommand selectCommand = new SqlCommand(selectStatement, connection);

            try
            {
                connection.Open();
                SqlDataReader reader = selectCommand.ExecuteReader();
                while (reader.Read())
                {
                    FurnitureListing aListing = new FurnitureListing();
                    aListing.description = reader["description"].ToString();
                    listing.Add(aListing);
                }
                reader.Close();
            }
            catch (SqlException ex)
            {
                throw ex;
            }
            finally
            {
                connection.Close();
            }
            return listing;
        }

        //Returns smallest id
        public static int getMinID()
        {
            int min;
            SqlConnection connection = RentalDBA.GetConnection();
            string selectStatement =
                "SELECT MIN(furnitureID) FROM furniture ";

            SqlCommand selectCommand = new SqlCommand(selectStatement, connection);

            try
            {
                connection.Open();
                selectCommand.ExecuteNonQuery();
                min = (int)selectCommand.ExecuteScalar();
            }
            catch (SqlException ex)
            {
                throw ex;
            }
            finally
            {
                connection.Close();
            }
            return min;
        }

        //Returns largest id
        public static int getMaxID()
        {
            int max;
            SqlConnection connection = RentalDBA.GetConnection();
            string selectStatement =
                "SELECT MAX(furnitureID) FROM furniture ";

            SqlCommand selectCommand = new SqlCommand(selectStatement, connection);

            try
            {
                connection.Open();
                selectCommand.ExecuteNonQuery();
                max = (int)selectCommand.ExecuteScalar();
            }
            catch (SqlException ex)
            {
                throw ex;
            }
            finally
            {
                connection.Close();
            }
            return max;
        }

        //Returns furniture item matching passed serial
        public static FurnitureListing getItemBySerialNumber(int serialNumber)
        {
            FurnitureListing item = new FurnitureListing();

            SqlConnection connection = RentalDBA.GetConnection();
            string selectStatement =
                "SELECT serialNumber, c.categoryName, s.styleName, " +
                "f.description, f.dailyRentalRate, f.fineRate " +
                "FROM furniture f " +
                "JOIN furnitureItem i ON f.furnitureID = i.furnitureID " +
                "JOIN category c ON f.categoryID = c.categoryID " +
                "JOIN style s ON f.styleID = s.styleID " +
                "WHERE i.serialNumber = @serialNumber";

            SqlCommand selectCommand = new SqlCommand(selectStatement, connection);
            selectCommand.Parameters.AddWithValue("@serialNumber", serialNumber);

            try
            {
                connection.Open();
                SqlDataReader reader = selectCommand.ExecuteReader();
                int rate = reader.GetOrdinal("dailyRentalRate");
                int fine = reader.GetOrdinal("fineRate");

                while (reader.Read())
                {
                    item.serialNumber = Convert.ToInt32(reader["serialNumber"]);
                    item.category = reader["categoryName"].ToString();
                    item.style = reader["styleName"].ToString();
                    item.description = reader["description"].ToString();
                    item.dailyRate = string.Format("${0:0.00}", (double)reader.GetDecimal(rate));
                    item.dailyFine = string.Format("${0:0.00}", (double)reader.GetDecimal(fine));
                }
                reader.Close();
            }
            catch (SqlException ex)
            {
                throw ex;
            }
            finally
            {
                connection.Close();
            }
            return item;

        }
    }
}
