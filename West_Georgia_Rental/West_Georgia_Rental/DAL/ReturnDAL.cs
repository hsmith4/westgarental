﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using West_Georgia_Rental.Model;

namespace West_Georgia_Rental.DAL
{
    public static class ReturnDAL
    {
        //Creates a new return id using passed employee id and current date
        public static int returnLineItem(int employeeID)
        {
            int returnID = 0;

            SqlConnection connection = RentalDBA.GetConnection();
            string insertStatement =
                "INSERT into [return] " +
                "(empID, dateReturned) " +
                "VALUES (@employeeID, getdate())";
            SqlCommand insertCommand = new SqlCommand(insertStatement, connection);
            insertCommand.Parameters.AddWithValue("@employeeID", employeeID);

            try
            {
                connection.Open();
                insertCommand.ExecuteNonQuery();
                string selectStatement =
                    "SELECT IDENT_CURRENT('[return]') FROM [return]";
                SqlCommand selectCommand = new SqlCommand(selectStatement, connection);
                returnID = Convert.ToInt32(selectCommand.ExecuteScalar());

            }
            catch (SqlException)
            {
                throw;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                if (connection != null)
                    connection.Close();
            }
            return returnID;
        }

        //Returns a list of all returns for the passed member
        public static List<Return> getReturnsByMember(int memberID)
        {
            List<Return> returns = new List<Return>();
            SqlConnection connection = RentalDBA.GetConnection();

            string selectStatement = "SELECT r.rentalID, c.categoryName, s.styleName, f.description, ri.dueDate, " +
                "re.dateReturned, re.empID " +
                "FROM [return] re " +
                "JOIN rentalLineItem ri ON re.returnID = ri.returnID " +
                "JOIN rental r ON ri.rentalID = r.rentalID " +
                "JOIN furnitureItem fi ON ri.serialNumber = fi.serialNumber " +
                "JOIN furniture f ON fi.furnitureID = f.furnitureID " +
                "JOIN category c ON f.categoryID = c.categoryID " +
                "JOIN style s ON f.styleID = s.styleID " +
                "JOIN employee e ON r.empID = e.empID " +
                "WHERE r.memberID = @memberID";

            SqlCommand selectCommand = new SqlCommand(selectStatement, connection);
            selectCommand.Parameters.AddWithValue("@memberID", memberID);

            SqlDataReader reader = null;
            try
            {
                connection.Open();
                reader = selectCommand.ExecuteReader();
                
                while (reader.Read())
                {
                    Return aReturn = new Return();

                    aReturn.dailyRate = reader["rentalID"].ToString();
                    aReturn.dailyFine = employeeName((int)reader["empID"]);
                    aReturn.category = reader["categoryName"].ToString();
                    aReturn.style = reader["styleName"].ToString();
                    aReturn.description = reader["description"].ToString();
                    aReturn.dueDate = (DateTime)reader["dueDate"];
                    aReturn.returnDate = (DateTime)reader["dateReturned"];

                    if (aReturn.returnDate > aReturn.dueDate)
                    {
                        aReturn.overDue = "Yes";
                    }
                    else
                    {
                        aReturn.overDue = "No";
                    }
                    returns.Add(aReturn);
                }
                reader.Close();
            }
            catch (SqlException)
            {
                throw;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                if (connection != null)
                    connection.Close();
                if (reader != null)
                    reader.Close();
            }
            return returns;
        }

        //Returns the employee name corresponding to the passed id
        private static string employeeName(int empID)
        {
            string employee = "";
            
            SqlConnection connection = RentalDBA.GetConnection();

            string selectStatement = "SELECT CONCAT(e.fName, ' ', e.lName) AS Employee " +
                "FROM [return] re " +
                "JOIN employee e ON re.empID = e.empID " +
                "WHERE re.empID = @empID";

            SqlCommand selectCommand = new SqlCommand(selectStatement, connection);
            selectCommand.Parameters.AddWithValue("@empID", empID);

            SqlDataReader reader = null;
            try
            {
                connection.Open();
                reader = selectCommand.ExecuteReader();
                int emp = reader.GetOrdinal("Employee");
                while (reader.Read())
                {
                    employee = reader.GetString(emp);
                }
                reader.Close();
            }
            catch (SqlException)
            {
                throw;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                if (connection != null)
                    connection.Close();
                if (reader != null)
                    reader.Close();
            }
            return employee;
        }
    }
}
