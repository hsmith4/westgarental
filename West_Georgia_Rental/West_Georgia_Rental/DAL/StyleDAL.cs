﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using West_Georgia_Rental.Model;

namespace West_Georgia_Rental.DAL
{
    public static class StyleDAL
    {
        //Returns a list of all furniture styles
        public static List<FurnitureListing> getAllStyles()
        {
            List<FurnitureListing> listing = new List<FurnitureListing>();

            SqlConnection connection = RentalDBA.GetConnection();
            string selectStatement =
                "SELECT DISTINCT styleName " +
                "FROM style ";

            SqlCommand selectCommand = new SqlCommand(selectStatement, connection);

            try
            {
                connection.Open();
                SqlDataReader reader = selectCommand.ExecuteReader();
                while (reader.Read())
                {
                    FurnitureListing aListing = new FurnitureListing();
                    aListing.style = reader["styleName"].ToString();
                    listing.Add(aListing);
                }
                reader.Close();
            }
            catch (SqlException ex)
            {
                throw ex;
            }
            finally
            {
                connection.Close();
            }
            return listing;
        }
    }
}
