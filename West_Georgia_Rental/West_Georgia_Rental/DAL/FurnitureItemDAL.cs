﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using West_Georgia_Rental.Model;

namespace West_Georgia_Rental.DAL
{
    public static class FurnitureItemDAL
    {
        //Returns the smallest serial number
        public static int getMinSerial()
        {
            int min;
            SqlConnection connection = RentalDBA.GetConnection();
            string selectStatement =
                "SELECT MIN(serialNumber) FROM furnitureItem ";

            SqlCommand selectCommand = new SqlCommand(selectStatement, connection);

            try
            {
                connection.Open();
                selectCommand.ExecuteNonQuery();
                min = (int)selectCommand.ExecuteScalar();
            }
            catch (SqlException ex)
            {
                throw ex;
            }
            finally
            {
                connection.Close();
            }
            return min;
        }

        //Returns the largest serial number
        public static int getMaxSerial()
        {
            int max;
            SqlConnection connection = RentalDBA.GetConnection();
            string selectStatement =
                "SELECT MAX(serialNumber) FROM furnitureItem ";

            SqlCommand selectCommand = new SqlCommand(selectStatement, connection);

            try
            {
                connection.Open();
                selectCommand.ExecuteNonQuery();
                max = (int)selectCommand.ExecuteScalar();
            }
            catch (SqlException ex)
            {
                throw ex;
            }
            finally
            {
                connection.Close();
            }
            return max;
        }

        //Returns the furniture item with passed id
        public static FurnitureListing getItemListing(int serialNumber)
        {
            FurnitureListing item = new FurnitureListing();
            SqlConnection connection = RentalDBA.GetConnection();
            string selectStatement = "SELECT * FROM furnitureItem " +
                "inner join furniture " +
                "on furnitureItem.furnitureID = furniture.furnitureID " +
                "WHERE serialNumber = @SerialNumber";
            SqlCommand selectCommand = new SqlCommand(selectStatement, connection);
            selectCommand.Parameters.AddWithValue("@SerialNumber", serialNumber);
            SqlDataReader reader = null;
            try
            {
                connection.Open();
                int dailyRate = reader.GetOrdinal("dailyRentalRate");
                int dailyFine = reader.GetOrdinal("fineRate");
                reader = selectCommand.ExecuteReader(System.Data.CommandBehavior.SingleRow);
                if (reader.Read())
                {
                    item.serialNumber = (int)reader["serialNumber"];
                    item.category = reader["category"].ToString();
                    item.style = reader["style"].ToString();
                    item.description = reader["description"].ToString();
                    item.dailyRate = string.Format("${0:0.00}", (double)reader.GetDecimal(dailyRate));
                    item.dailyFine = string.Format("${0:0.00}", (double)reader.GetDecimal(dailyFine));
                }
                else
                {
                   item = null;
                }
            }
            catch (SqlException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (connection != null)
                    connection.Close();
                if (reader != null)
                    reader.Close();
            }
            return item;

        }

        //Checks to see if the item with passed serial is available to rent
        public static int checkItemAvailability(int serialNumber)
        {
            int available;
            SqlConnection connection = RentalDBA.GetConnection();
            string selectStatement = "SELECT * FROM rentalLineItem rli " +
                                        "FULL OUTER JOIN cart c " +
                                        "ON rli.serialNumber = c.serialNumber " +
                                        "LEFT JOIN furnitureItem fi " +
                                        "ON rli.serialNumber = fi.serialNumber " +
                                        "OR c.serialNumber = fi.serialNumber " +
                                        "WHERE fi.serialNumber = 123 " +
                                        "AND(c.serialNumber is not null " +
                                        "OR(rli.serialNumber is not null AND rli.returnID is null))";
            SqlCommand selectCommand = new SqlCommand(selectStatement, connection);
            selectCommand.Parameters.AddWithValue("@SerialNumber", serialNumber);
            SqlDataReader reader = null;
            try
            {
                connection.Open();
                available = Convert.ToInt32(selectCommand.ExecuteScalar());
            }
            catch (SqlException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (connection != null)
                    connection.Close();
                if (reader != null)
                    reader.Close();
            }
            return available;
        }

        //Returns true if item is available
        public static bool isItemAvailable(int serialNumber)
        {
            string theItem;
            bool available = false;
            SqlConnection connection = RentalDBA.GetConnection();
            string selectStatement = "SELECT ri.serialNumber " +
                "FROM furnitureItem fi " +
                "JOIN rentalLineItem ri ON fi.serialNumber = ri.serialNumber " +
                "WHERE ri.serialNumber = @SerialNumber AND ri.returnID IS NULL";
            SqlCommand selectCommand = new SqlCommand(selectStatement, connection);
            selectCommand.Parameters.AddWithValue("@SerialNumber", serialNumber);
            SqlDataReader reader = null;
            try
            {
                connection.Open();
                theItem = Convert.ToString(selectCommand.ExecuteScalar());

                if (theItem == "")
                {
                    available = true;
                }
            }
            catch (SqlException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (connection != null)
                    connection.Close();
                if (reader != null)
                    reader.Close();
            }
            return available;
        }
    }
}
