﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using West_Georgia_Rental.Model;

namespace West_Georgia_Rental.DAL
{
    public static class MemberDAL
    {
        //Returns a list of all members
        public static List<Member> getMembers()
        {
            List<Member> members = new List<Member>();
            SqlConnection connection = RentalDBA.GetConnection();
            string selectStatement = "SELECT * " +
                "FROM Member " +
                "ORDER BY Name";
            SqlCommand selectCommand = new SqlCommand(selectStatement, connection);
            SqlDataReader reader = null;
            try
            {
                connection.Open();
                reader = selectCommand.ExecuteReader();
                while (reader.Read())
                {
                    Member member = new Member();
                    member.memberID = (int)reader["memberID"];
                    member.DOB = (DateTime)reader["DOB"];
                    member.fName = reader["fName"].ToString();
                    if (reader["minit"] != DBNull.Value)
                    {
                        member.minit = reader["minit"].ToString();
                    }
                    member.lName = reader["lName"].ToString();
                    member.phone = reader["phone"].ToString();
                    member.email = reader["email"].ToString();
                    member.address1 = reader["address1"].ToString();
                    member.address2 = reader["address2"].ToString();
                    member.city = reader["city"].ToString();
                    member.state = reader["state"].ToString();
                    member.zip = reader["zip"].ToString();

                    members.Add(member);
                }
                reader.Close();
            }
            catch (SqlException)
            {
                throw;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                if (connection != null)
                    connection.Close();
                if (reader != null)
                    reader.Close();
            }
            return members;
        }

        //Creates a new member listing based on the passed values
        public static int createMember(DateTime DOB, string fName, string minit, string lName, string phone, string email, string address1, string address2, string city, string state, string zip)
        {
            int memberID = 0;
            SqlConnection connection = RentalDBA.GetConnection();
            string insertStatement =
                "INSERT member " +
                "(DOB, fName, minit, lName, phone, email, address1, address2, city, state, zip) " +
                "VALUES " +
                "(@DOB, @fName, @minit, @lName, @phone, @email, @address1, @address2, @city, @state, @zip)";
            SqlCommand insertCommand = new SqlCommand(insertStatement, connection);
            insertCommand.Parameters.AddWithValue("@DOB", DOB);
            insertCommand.Parameters.AddWithValue("@fName", fName);
            if (minit == "")
                insertCommand.Parameters.AddWithValue("@minit", DBNull.Value);
            else
                insertCommand.Parameters.AddWithValue("@minit", minit);
            insertCommand.Parameters.AddWithValue("@lName", lName);
            insertCommand.Parameters.AddWithValue("@phone", phone);
            if (email == "")
                insertCommand.Parameters.AddWithValue("@email", DBNull.Value);
            else
                insertCommand.Parameters.AddWithValue("@email", email);
            if (address1 == "")
                insertCommand.Parameters.AddWithValue("@address1", DBNull.Value);
            else
                insertCommand.Parameters.AddWithValue("@address1", address1);
            if (address2 == "")
                insertCommand.Parameters.AddWithValue("@address2", DBNull.Value);
            else
                insertCommand.Parameters.AddWithValue("@address2", address2);
            if (city == "")
                insertCommand.Parameters.AddWithValue("@city", DBNull.Value);
            else
                insertCommand.Parameters.AddWithValue("@city", city);
            if (state == "")
                insertCommand.Parameters.AddWithValue("@state", DBNull.Value);
            else
                insertCommand.Parameters.AddWithValue("@state", state);
            if (zip == "")
                insertCommand.Parameters.AddWithValue("@zip", DBNull.Value);
            else
                insertCommand.Parameters.AddWithValue("@zip", zip);

            try
            {
                connection.Open();
                insertCommand.ExecuteNonQuery();
                string selectStatement =
                    "SELECT IDENT_CURRENT('member') FROM member";
                SqlCommand selectCommand = new SqlCommand(selectStatement, connection);
                memberID = Convert.ToInt32(selectCommand.ExecuteScalar());

            }
            catch (SqlException)
            {
                throw;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                if (connection != null)
                    connection.Close();
            }
            return memberID;
        }

        //Returns a member based on their phone or complete name
        public static List<Member> getMemberByPhoneOrName(string phone, string first, string last)
        {
            List<Member> members = new List<Member>();
            SqlConnection connection = RentalDBA.GetConnection();
            string selectStatement = "SELECT memberID, fName, lName, email, phone, address1, address2," +
                "city, state, zip " +
                "FROM Member " +
                "WHERE phone = @phone OR (fName = @first AND lName = @last)"; 

            SqlCommand selectCommand = new SqlCommand(selectStatement, connection);
            selectCommand.Parameters.AddWithValue("@phone", phone);
            selectCommand.Parameters.AddWithValue("@first", first);
            selectCommand.Parameters.AddWithValue("@last", last);

            SqlDataReader reader = null;
            try
            {
                connection.Open();
                reader = selectCommand.ExecuteReader();
                while (reader.Read())
                {
                    Member member = new Member();
                    member.memberID = (int)reader["memberID"];
                    member.fName = reader["fName"].ToString();
                    member.lName = reader["lName"].ToString();
                    member.email = reader["email"].ToString();
                    member.phone = reader["phone"].ToString();
                    member.address1 = reader["address1"].ToString();
                    member.address2 = reader["address2"].ToString();
                    member.city = reader["city"].ToString();
                    member.state = reader["state"].ToString();
                    member.zip = reader["zip"].ToString();

                    members.Add(member);
                }
                reader.Close();
            }
            catch (SqlException)
            {
                throw;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                if (connection != null)
                    connection.Close();
                if (reader != null)
                    reader.Close();
            }
            return members;
        }

        //Returns all members ordered by last name
        public static List<Member> getAllMembers()
        {
            List<Member> members = new List<Member>();
            SqlConnection connection = RentalDBA.GetConnection();
            string selectStatement = "SELECT memberID, fName, lName, email, phone, address1, address2," +
                "city, state, zip " +
                "FROM Member " +
                "ORDER BY lName";
            SqlCommand selectCommand = new SqlCommand(selectStatement, connection);

            SqlDataReader reader = null;
            try
            {
                connection.Open();
                reader = selectCommand.ExecuteReader();
                while (reader.Read())
                {
                    Member member = new Member();
                    member.memberID = (int)reader["memberID"];
                    member.fName = reader["fName"].ToString();
                    member.lName = reader["lName"].ToString();
                    member.email = reader["email"].ToString();
                    member.phone = reader["phone"].ToString();
                    member.address1 = reader["address1"].ToString();
                    member.address2 = reader["address2"].ToString();
                    member.city = reader["city"].ToString();
                    member.state = reader["state"].ToString();
                    member.zip = reader["zip"].ToString();

                    members.Add(member);
                }
                reader.Close();
            }
            catch (SqlException)
            {
                throw;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                if (connection != null)
                    connection.Close();
                if (reader != null)
                    reader.Close();
            }
            return members;
        }

        //Returns the member with the passed id
        public static Member getMemberByID(int memberID)
        {
            Member member = new Member();
            SqlConnection connection = RentalDBA.GetConnection();
            string selectStatement = "SELECT *  FROM member " +
                "WHERE memberID = @MemberID";
            SqlCommand selectCommand = new SqlCommand(selectStatement, connection);
            selectCommand.Parameters.AddWithValue("@MemberID", memberID);
            SqlDataReader reader = null;
            try
            {
                connection.Open();
                reader = selectCommand.ExecuteReader(System.Data.CommandBehavior.SingleRow);
                if (reader.Read())
                {
                    member.memberID = (int)reader["memberID"];
                    member.fName = reader["fName"].ToString();
                    if (reader["minit"] != DBNull.Value)
                        member.minit = reader["minit"].ToString();
                    else
                        member.minit = "";
                    member.lName = reader["lName"].ToString();
                    member.DOB = (DateTime)reader["DOB"];
                    member.email = reader["email"].ToString();
                    member.phone = reader["phone"].ToString();
                    member.address1 = reader["address1"].ToString();
                    if (reader["address2"] != DBNull.Value)
                        member.address2 = reader["address2"].ToString();
                    else
                        member.address2 = "";
                    member.city = reader["city"].ToString();
                    member.state = reader["state"].ToString();
                    member.zip = reader["zip"].ToString();
                }
                else
                {
                    member = null;
                }
            }
            catch (SqlException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (connection != null)
                    connection.Close();
                if (reader != null)
                    reader.Close();
            }
            return member;
        }

        //Returns true if the member was successfully updated
        public static bool updateMember(int memberID, string fName, string minit, string lName, DateTime DOB, string phone, string email, string address1, string address2, string city, string state, string zip)
        {
            SqlConnection connection = RentalDBA.GetConnection();
            string updateStatement =
                "UPDATE member set " +
                "DOB = @DOB, " +
                "fName = @FirstName, " +
                "minit = @Minit, " +
                "lName = @LastName, " +
                "phone = @Phone, " +
                "email = @Email, " +
                "address1 = @Address1, " +
                "address2 = @Address2, " +
                "city = @City, " +
                "state = @State, " +
                "zip = @Zip " +
                "WHERE memberID = @MemberID";
            SqlCommand updateCommand = new SqlCommand(updateStatement, connection);
            updateCommand.Parameters.AddWithValue("@MemberID", memberID);
            updateCommand.Parameters.AddWithValue("@DOB", DOB);
            updateCommand.Parameters.AddWithValue("@FirstName", fName);
            if (minit == "")
                updateCommand.Parameters.AddWithValue("@Minit", DBNull.Value);
            else
                updateCommand.Parameters.AddWithValue("@Minit", minit);
            updateCommand.Parameters.AddWithValue("@LastName", lName);
            updateCommand.Parameters.AddWithValue("@Phone", phone);
            updateCommand.Parameters.AddWithValue("@Email", email);
            updateCommand.Parameters.AddWithValue("@Address1", address1);
            if (address2 == "")
                updateCommand.Parameters.AddWithValue("@Address2", DBNull.Value);
            else
                updateCommand.Parameters.AddWithValue("@Address2", address2);
            updateCommand.Parameters.AddWithValue("@City", city);
            updateCommand.Parameters.AddWithValue("@State", state);
            updateCommand.Parameters.AddWithValue("@Zip", zip);

            SqlDataReader reader = null;
            try
            {
                connection.Open();
                int count = updateCommand.ExecuteNonQuery();
                if (count > 0)
                    return true;
                else
                    return false;
            }
            catch (SqlException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (connection != null)
                    connection.Close();
                if (reader != null)
                    reader.Close();
            }
        }

        //Deletes the member with the passed id
        public static int deleteMember(int memberID)
        {
            int rowCount = 0;
            SqlConnection connection = RentalDBA.GetConnection();
            string deleteStatement = "DELETE FROM member " +
                "WHERE memberID = @MemberID";
            SqlCommand deleteCommand = new SqlCommand(deleteStatement, connection);
            deleteCommand.Parameters.AddWithValue("@MemberID", memberID);
            SqlDataReader reader = null;
            try
            {
                connection.Open();
                rowCount = deleteCommand.ExecuteNonQuery();
            }
            catch (SqlException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (connection != null)
                    connection.Close();
                if (reader != null)
                    reader.Close();
            }
            return rowCount;
        }
    }
}