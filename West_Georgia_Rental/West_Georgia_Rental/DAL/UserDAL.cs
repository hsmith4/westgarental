﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using West_Georgia_Rental.Model;

namespace West_Georgia_Rental.DAL
{
    class UserDAL
    {
        //Returns the current user based on the passed parameters
        public static User getUserAtLogin(string username, string password)
        {
            User user = new User();
            SqlConnection connection = RentalDBA.GetConnection();
            string selectStatement = "SELECT * FROM empSystem " +
                "WHERE username = @Username " +
                "AND password = @Password";
            SqlCommand selectCommand = new SqlCommand(selectStatement, connection);
            selectCommand.Parameters.AddWithValue("@Username", username);
            selectCommand.Parameters.AddWithValue("@Password", password);
            SqlDataReader reader = null;
            try
            {
                connection.Open();
                reader = selectCommand.ExecuteReader(System.Data.CommandBehavior.SingleRow);
                if (reader.Read())
                {
                    user.empID = Convert.ToInt32(reader["empID"]);
                    user.username = reader["username"].ToString();
                    user.password = reader["password"].ToString();
                    user.inAdmin = Convert.ToInt32(reader["inAdmin"]);
                }
                else
                {
                    user = null;
                }
            }
            catch (SqlException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (connection != null)
                    connection.Close();
                if (reader != null)
                    reader.Close();
            }
            return user;
        }

        //Updates the password for the passed user to the passed password
        public static void updatePassword(string username, string password)
        {
            SqlConnection connection = RentalDBA.GetConnection();
            string updateStatement = "UPDATE empSystem " +
                "SET password = @Password " +
                "WHERE userName = @Username";
            SqlCommand updateCommand = new SqlCommand(updateStatement, connection);
            updateCommand.Parameters.AddWithValue("@Username", username);
            updateCommand.Parameters.AddWithValue("@Password", password);
            SqlDataReader reader = null;
            try
            {
                connection.Open();
                updateCommand.ExecuteNonQuery();
            }
            catch (SqlException)
            {
                throw;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                if (connection != null)
                    connection.Close();
                if (reader != null)
                    reader.Close();
            }
        }

        //Returns the password for the passed user
        public static String getPassword(string username)
        {
            string password = "";
            SqlConnection connection = RentalDBA.GetConnection();
            string selectStatement = "SELECT password " +
                "FROM empSystem " +
                "WHERE userName = @Username";
            SqlCommand selectCommand = new SqlCommand(selectStatement, connection);
            selectCommand.Parameters.AddWithValue("@Username", username);
            
            SqlDataReader reader = null;
            try
            {
                connection.Open();
                reader = selectCommand.ExecuteReader(System.Data.CommandBehavior.SingleRow);

                while (reader.Read())
                {
                    password = reader["password"].ToString();
                }
            }
            catch (SqlException)
            {
                throw;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                if (connection != null)
                    connection.Close();
                if (reader != null)
                    reader.Close();
            }
            return password;
        }
    }
}
