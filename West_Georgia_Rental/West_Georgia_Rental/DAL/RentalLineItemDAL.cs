﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using West_Georgia_Rental.Model;

namespace West_Georgia_Rental.DAL
{
    public static class RentalLineItemDAL
    {
        //Returns a list of all open rentals for the passed member
        public static List<Rental> getRentalsByID(int memberID)
        {
            List<Rental> rentals = new List<Rental>();
            SqlConnection connection = RentalDBA.GetConnection();

            string selectStatement = "SELECT ri.serialNumber, f.furnitureID, ri.rentalID, c.categoryName, s.styleName, f.description, f.dailyRentalRate, " +
                "f.fineRate, ri.dueDate " +
                "FROM rentalLineItem ri " +
                "JOIN rental r ON ri.rentalID = r.rentalID " +
                "JOIN furnitureItem fi ON ri.serialNumber = fi.serialNumber " +
                "JOIN furniture f ON fi.furnitureID = f.furnitureID " +
                "JOIN category c ON f.categoryID = c.categoryID " +
                "JOIN style s ON f.styleID = s.styleID " +
                "WHERE ri.returnID IS NULL AND r.memberID = @memberID";

            SqlCommand selectCommand = new SqlCommand(selectStatement, connection);
            selectCommand.Parameters.AddWithValue("@memberID", memberID);

            SqlDataReader reader = null;
            try
            {
                connection.Open();
                reader = selectCommand.ExecuteReader();
                while (reader.Read())
                {
                    Rental rental = new Rental();
                    rental.serialNumber = Convert.ToInt32(reader["serialNumber"]);
                    rental.furnitureID = Convert.ToInt32(reader["furnitureID"]);
                    rental.rentalID = (int)reader["rentalID"];
                    rental.category = reader["categoryName"].ToString();
                    rental.style = reader["styleName"].ToString();
                    rental.description = reader["description"].ToString();
                    if (reader["dueDate"] != DBNull.Value)
                        rental.dueDate = (DateTime)reader["dueDate"];
                    else
                        rental.dueDate = DateTime.MaxValue;
                    rental.dailyRate = string.Format("{0:0.00}", (double)((decimal)reader["dailyRentalRate"]));
                    rental.dailyFine = string.Format("{0:0.00}", (double)((decimal)reader["fineRate"]));
                    
                    rentals.Add(rental);
                }
                reader.Close();
            }
            catch (SqlException)
            {
                throw;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                if (connection != null)
                    connection.Close();
                if (reader != null)
                    reader.Close();
            }
            return rentals;
        }

        //Returns true if the update was successfully completed
        public static bool updateLineItemRental(int rentalID, int returnID, int serialNumber)
        {
            SqlConnection connection = RentalDBA.GetConnection();
            string updateStatement =
                "UPDATE rentalLineItem set " +
                "returnID = @returnID " +
                "WHERE rentalID = @rentalID " +
                "AND serialNumber = @serialNumber";
            SqlCommand updateCommand = new SqlCommand(updateStatement, connection);

            updateCommand.Parameters.AddWithValue("@returnID", returnID);
            updateCommand.Parameters.AddWithValue("@rentalID", rentalID);
            updateCommand.Parameters.AddWithValue("@serialNumber", serialNumber);

            SqlDataReader reader = null;
            try
            {
                connection.Open();
                int count = updateCommand.ExecuteNonQuery();
                if (count > 0)
                    return true;
                else
                    return false;
            }
            catch (SqlException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (connection != null)
                    connection.Close();
                if (reader != null)
                    reader.Close();
            }
        }

        //Returns the rental corresponding to the passed rental id and serial
        public static Rental getRentalItemBySerialNumber(int rentalID, int serialNumber)
        {
            Rental rental = new Rental();
            SqlConnection connection = RentalDBA.GetConnection();

            string selectStatement = "SELECT ri.serialNumber, ri.rentalID, c.categoryName, s.styleName, f.description, f.dailyRentalRate, " +
                "f.fineRate, ri.dueDate " +
                "FROM rentalLineItem ri " +
                "JOIN rental r ON ri.rentalID = r.rentalID " +
                "JOIN furnitureItem fi ON ri.serialNumber = fi.serialNumber " +
                "JOIN furniture f ON fi.furnitureID = f.furnitureID " +
                "JOIN category c ON f.categoryID = c.categoryID " +
                "JOIN style s ON f.styleID = s.styleID " +
                "WHERE r.rentalID = @rentalID " +
                "AND ri.serialNumber = @serialNumber";

            SqlCommand selectCommand = new SqlCommand(selectStatement, connection);
            selectCommand.Parameters.AddWithValue("@rentalID", rentalID);
            selectCommand.Parameters.AddWithValue("@serialNumber", serialNumber);

            SqlDataReader reader = null;
            try
            {
                connection.Open();
                reader = selectCommand.ExecuteReader();
                while (reader.Read())
                {
                    rental.serialNumber = Convert.ToInt32(reader["serialNumber"]);
                    rental.rentalID = (int)reader["rentalID"];
                    rental.category = reader["categoryName"].ToString();
                    rental.style = reader["styleName"].ToString();
                    rental.description = reader["description"].ToString();
                    if (reader["dueDate"] != DBNull.Value)
                        rental.dueDate = (DateTime)reader["dueDate"];
                    else
                        rental.dueDate = DateTime.MaxValue;
                    rental.dailyRate = string.Format("{0:0.00}", (double)((decimal)reader["dailyRentalRate"]));
                    rental.dailyFine = string.Format("{0:0.00}", (double)((decimal)reader["fineRate"]));
                }
                reader.Close();
            }
            catch (SqlException)
            {
                throw;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                if (connection != null)
                    connection.Close();
                if (reader != null)
                    reader.Close();
            }
            return rental;
        }

        //Creates a new rental line item based on passed id, serial, and member
        public static int createRentalLineItem(int rentalID, int serialNumber, int memberID)
        {
            int created = 0;
            SqlConnection connection = RentalDBA.GetConnection();
            string insertStatement =
                "INSERT into rentalLineItem " +
                "(rentalID, serialNumber, returnID, dueDate) " +
                    "(SELECT @RentalID, @SerialNumber, null, DATEADD(day, lengthOfRental, GETDATE()) " +
                    "FROM cart " +
                    "WHERE memberID = @MemberID " +
                    "AND serialNumber = @SerialNumber)";
            SqlCommand insertCommand = new SqlCommand(insertStatement, connection);
            insertCommand.Parameters.AddWithValue("@RentalID", rentalID);
            insertCommand.Parameters.AddWithValue("@SerialNumber", serialNumber);
            insertCommand.Parameters.AddWithValue("@MemberID", memberID);

            try
            {
                connection.Open();
                created = insertCommand.ExecuteNonQuery();
            }
            catch (SqlException)
            {
                throw;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                if (connection != null)
                    connection.Close();
            }
            return created;
        }

        //Returns the current rental status based on id and serial
        public static string getRentalStatus(int rentalID, int serialNumber)
        {
            string status = "";
            SqlConnection connection = RentalDBA.GetConnection();
            string selectStatement = "SELECT CASE " +
                                        "WHEN returnID is null and getdate() < dueDate THEN 'NO' " +
                                        "WHEN returnID is null and getdate() > dueDate THEN 'YES' " +
                                        "END AS [status] " +
                                    "FROM rentalLineItem " +
                                    "WHERE rentalID = @RentalID " +
                                    "AND serialNumber = @SerialNumber";
            SqlCommand selectCommand = new SqlCommand(selectStatement, connection);
            selectCommand.Parameters.AddWithValue("@RentalID", rentalID);
            selectCommand.Parameters.AddWithValue("@SerialNumber", serialNumber);
            SqlDataReader reader = null;
            try
            {
                connection.Open();
                status = selectCommand.ExecuteScalar().ToString();
            }
            catch (SqlException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (connection != null)
                    connection.Close();
                if (reader != null)
                    reader.Close();
            }

            return status;
        }
    }
}
