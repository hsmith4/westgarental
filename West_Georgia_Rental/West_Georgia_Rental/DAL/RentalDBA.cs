﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace West_Georgia_Rental.DAL
{
    public static class RentalDBA
    {
        //This method returns the SQL connection for the database
        public static SqlConnection GetConnection()
        {

            string connectionString =
            "Data Source=localhost;Initial Catalog=CS6232-G3;" +
            "Integrated Security=True";
            SqlConnection connection = new SqlConnection(connectionString);

            return connection;
        }
    }
}
