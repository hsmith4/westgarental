﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace West_Georgia_Rental.Model
{
    public class Member
    {
        public int memberID { get; set; }
        public string fName { get; set; }
        public string minit { get; set; }
        public string lName { get; set; }
        public DateTime DOB { get; set; }
        public string phone { get; set; }
        public string email { get; set; }
        public string address1 { get; set; }
        public string address2 { get; set; }
        public string city { get; set; }
        public string state { get; set; }
        public string zip { get; set; }

    }
}
