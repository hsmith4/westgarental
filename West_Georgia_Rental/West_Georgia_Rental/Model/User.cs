﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace West_Georgia_Rental.Model
{
    public class User
    {
        public string username { get; set; }

        public string password { get; set; }

        public int empID { get; set; }

        public int inAdmin { get; set; }

    }
}
