﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace West_Georgia_Rental.Model
{
    public class Rental
    {
        public int rentalID { get; set; }
        public string category { get; set; }
        public string style { get; set; }
        public string description { get; set; }
        public DateTime dueDate { get; set; }
        public string dailyRate { get; set; }
        public string dailyFine { get; set; }
        public int serialNumber { get; set; }
        public int furnitureID { get; set; }
    }
}
