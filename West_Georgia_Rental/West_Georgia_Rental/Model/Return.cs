﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace West_Georgia_Rental.Model
{
    public class Return
    {
        public int rentalID { get; set; }
        public string category { get; set; }
        public string style { get; set; }
        public string description { get; set; }
        public DateTime dueDate { get; set; }
        public DateTime returnDate { get; set; }
        public string overDue { get; set; }
        public string dailyFine { get; set; }
        public string dailyRate { get; set; }
    }
}
