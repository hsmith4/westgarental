﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace West_Georgia_Rental.Model
{
    public class Cart
    {
        public int serialNumber { get; set; }
        public string fName { get; set; }
        public string lName { get; set; }
        public int furnitureID { get; set; }
        public string categoryName { get; set; }
        public string description { get; set; }
        public string styleName { get; set; }
        public DateTime returnDate { get; set; }
        public string dailyRate { get; set; }
        public int lengthOfRental { get; set; }
    }
}
