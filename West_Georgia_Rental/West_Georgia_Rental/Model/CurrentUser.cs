﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace West_Georgia_Rental.Model
{
    class CurrentUser
    {
        public static User User { get; private set; }
        

        public static void setCurrentUser(User user)
        {
            User = user;
        }
    }
}
