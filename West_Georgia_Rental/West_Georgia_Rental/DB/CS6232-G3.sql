USE [master]
GO
/****** Object:  Database [CS6232-G3]    Script Date: 4/10/18 11:13:43 AM ******/
CREATE DATABASE [CS6232-G3]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'CS6232-G3', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL14.MSSQLSERVER\MSSQL\DATA\CS6232-G3.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'CS6232-G3_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL14.MSSQLSERVER\MSSQL\DATA\CS6232-G3_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
GO
ALTER DATABASE [CS6232-G3] SET COMPATIBILITY_LEVEL = 140
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [CS6232-G3].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [CS6232-G3] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [CS6232-G3] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [CS6232-G3] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [CS6232-G3] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [CS6232-G3] SET ARITHABORT OFF 
GO
ALTER DATABASE [CS6232-G3] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [CS6232-G3] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [CS6232-G3] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [CS6232-G3] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [CS6232-G3] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [CS6232-G3] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [CS6232-G3] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [CS6232-G3] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [CS6232-G3] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [CS6232-G3] SET  DISABLE_BROKER 
GO
ALTER DATABASE [CS6232-G3] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [CS6232-G3] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [CS6232-G3] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [CS6232-G3] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [CS6232-G3] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [CS6232-G3] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [CS6232-G3] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [CS6232-G3] SET RECOVERY FULL 
GO
ALTER DATABASE [CS6232-G3] SET  MULTI_USER 
GO
ALTER DATABASE [CS6232-G3] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [CS6232-G3] SET DB_CHAINING OFF 
GO
ALTER DATABASE [CS6232-G3] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [CS6232-G3] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [CS6232-G3] SET DELAYED_DURABILITY = DISABLED 
GO
ALTER DATABASE [CS6232-G3] SET QUERY_STORE = OFF
GO
USE [CS6232-G3]
GO
ALTER DATABASE SCOPED CONFIGURATION SET IDENTITY_CACHE = ON;
GO
ALTER DATABASE SCOPED CONFIGURATION SET LEGACY_CARDINALITY_ESTIMATION = OFF;
GO
ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET LEGACY_CARDINALITY_ESTIMATION = PRIMARY;
GO
ALTER DATABASE SCOPED CONFIGURATION SET MAXDOP = 0;
GO
ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET MAXDOP = PRIMARY;
GO
ALTER DATABASE SCOPED CONFIGURATION SET PARAMETER_SNIFFING = ON;
GO
ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET PARAMETER_SNIFFING = PRIMARY;
GO
ALTER DATABASE SCOPED CONFIGURATION SET QUERY_OPTIMIZER_HOTFIXES = OFF;
GO
ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET QUERY_OPTIMIZER_HOTFIXES = PRIMARY;
GO
USE [CS6232-G3]
GO
/****** Object:  Table [dbo].[cart]    Script Date: 4/10/18 11:13:43 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cart](
	[memberID] [int] NOT NULL,
	[serialNumber] [int] NOT NULL,
	[empID] [int] NOT NULL,
	[lengthOfRental] [int] NULL,
 CONSTRAINT [PK_cart] PRIMARY KEY CLUSTERED 
(
	[memberID] ASC,
	[serialNumber] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[category]    Script Date: 4/10/18 11:13:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[category](
	[categoryID] [int] IDENTITY(1,1) NOT NULL,
	[categoryName] [varchar](50) NOT NULL,
 CONSTRAINT [PK_category] PRIMARY KEY CLUSTERED 
(
	[categoryID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[employee]    Script Date: 4/10/18 11:13:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[employee](
	[empID] [int] IDENTITY(1,1) NOT NULL,
	[DOB] [date] NOT NULL,
	[fName] [varchar](50) NOT NULL,
	[minit] [char](1) NULL,
	[lName] [varchar](50) NOT NULL,
	[phone] [char](10) NOT NULL,
	[address1] [varchar](50) NOT NULL,
	[address2] [varchar](50) NULL,
	[city] [varchar](50) NOT NULL,
	[state] [char](2) NOT NULL,
	[zip] [char](5) NOT NULL,
	[SSN] [char](9) NOT NULL,
 CONSTRAINT [PK_employee] PRIMARY KEY CLUSTERED 
(
	[empID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[empSystem]    Script Date: 4/10/18 11:13:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[empSystem](
	[empID] [int] NOT NULL,
	[username] [varchar](25) NOT NULL,
	[password] [varchar](64) NOT NULL,
	[inAdmin] [bit] NOT NULL,
 CONSTRAINT [PK_empSystem] PRIMARY KEY CLUSTERED 
(
	[empID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[furniture]    Script Date: 4/10/18 11:13:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[furniture](
	[furnitureID] [int] IDENTITY(1,1) NOT NULL,
	[categoryID] [int] NOT NULL,
	[styleID] [int] NOT NULL,
	[description] [varchar](50) NOT NULL,
	[dailyRentalRate] [decimal](6, 2) NOT NULL,
	[fineRate] [decimal](6, 2) NOT NULL,
 CONSTRAINT [PK_furniture] PRIMARY KEY CLUSTERED 
(
	[furnitureID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[furnitureItem]    Script Date: 4/10/18 11:13:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[furnitureItem](
	[serialNumber] [int] NOT NULL,
	[furnitureID] [int] NOT NULL,
 CONSTRAINT [PK_furnitureItem] PRIMARY KEY CLUSTERED 
(
	[serialNumber] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[member]    Script Date: 4/10/18 11:13:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[member](
	[memberID] [int] IDENTITY(1,1) NOT NULL,
	[DOB] [date] NOT NULL,
	[fName] [varchar](50) NOT NULL,
	[minit] [char](1) NULL,
	[lName] [varchar](50) NOT NULL,
	[phone] [char](10) NOT NULL,
	[email] [varchar](50) NOT NULL,
	[address1] [varchar](50) NOT NULL,
	[address2] [varchar](50) NULL,
	[city] [varchar](50) NOT NULL,
	[state] [char](2) NOT NULL,
	[zip] [char](5) NOT NULL,
 CONSTRAINT [PK_member] PRIMARY KEY CLUSTERED 
(
	[memberID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[rental]    Script Date: 4/10/18 11:13:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[rental](
	[rentalID] [int] IDENTITY(1,1) NOT NULL,
	[memberID] [int] NOT NULL,
	[empID] [int] NOT NULL,
	[rentalDate] [datetime] NOT NULL,
 CONSTRAINT [PK_rental] PRIMARY KEY CLUSTERED 
(
	[rentalID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[rentalLineItem]    Script Date: 4/10/18 11:13:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[rentalLineItem](
	[rentalID] [int] NOT NULL,
	[serialNumber] [int] NOT NULL,
	[returnID] [int] NULL,
	[dueDate] [datetime] NULL,
 CONSTRAINT [PK_rentalLineItem] PRIMARY KEY CLUSTERED 
(
	[rentalID] ASC,
	[serialNumber] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[return]    Script Date: 4/10/18 11:13:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[return](
	[returnID] [int] IDENTITY(1,1) NOT NULL,
	[empID] [int] NOT NULL,
	[dateReturned] [date] NOT NULL,
 CONSTRAINT [PK_return] PRIMARY KEY CLUSTERED 
(
	[returnID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[style]    Script Date: 4/10/18 11:13:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[style](
	[styleID] [int] IDENTITY(1,1) NOT NULL,
	[styleName] [varchar](50) NOT NULL,
 CONSTRAINT [PK_style] PRIMARY KEY CLUSTERED 
(
	[styleID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[category] ON 

INSERT [dbo].[category] ([categoryID], [categoryName]) VALUES (1, N'Bed')
INSERT [dbo].[category] ([categoryID], [categoryName]) VALUES (2, N'Couch')
INSERT [dbo].[category] ([categoryID], [categoryName]) VALUES (3, N'Table')
INSERT [dbo].[category] ([categoryID], [categoryName]) VALUES (4, N'Chair')
SET IDENTITY_INSERT [dbo].[category] OFF
SET IDENTITY_INSERT [dbo].[employee] ON 

INSERT [dbo].[employee] ([empID], [DOB], [fName], [minit], [lName], [phone], [address1], [address2], [city], [state], [zip], [SSN]) VALUES (1, CAST(N'1979-04-13' AS Date), N'Mandi', N'A', N'Matchett', N'5555555555', N'1 Main St', NULL, N'Atlanta', N'GA', N'30303', N'111111111')
INSERT [dbo].[employee] ([empID], [DOB], [fName], [minit], [lName], [phone], [address1], [address2], [city], [state], [zip], [SSN]) VALUES (2, CAST(N'1981-03-04' AS Date), N'Hayden', N'E', N'Smith', N'5555555550', N'2 Main St', NULL, N'Atlanta', N'GA', N'30303', N'111111112')
INSERT [dbo].[employee] ([empID], [DOB], [fName], [minit], [lName], [phone], [address1], [address2], [city], [state], [zip], [SSN]) VALUES (3, CAST(N'1983-07-29' AS Date), N'Ronald', N'G', N'Williams', N'5555555551', N'3 Main St', NULL, N'Atlanta', N'GA', N'30303', N'111111113')
SET IDENTITY_INSERT [dbo].[employee] OFF
INSERT [dbo].[empSystem] ([empID], [username], [password], [inAdmin]) VALUES (1, N'MMatchett1', N'px??i?v&?mc??;DA?G.??q', 1)
INSERT [dbo].[empSystem] ([empID], [username], [password], [inAdmin]) VALUES (2, N'HSmith1', N'?!SCB??L0?????]N?? ?*???	cLh5', 1)
INSERT [dbo].[empSystem] ([empID], [username], [password], [inAdmin]) VALUES (3, N'RWilliams1', N'??e?????????F??#???Z??`?By??_', 0)
SET IDENTITY_INSERT [dbo].[furniture] ON 

INSERT [dbo].[furniture] ([furnitureID], [categoryID], [styleID], [description], [dailyRentalRate], [fineRate]) VALUES (1, 1, 1, N'Twin', CAST(25.00 AS Decimal(6, 2)), CAST(27.50 AS Decimal(6, 2)))
INSERT [dbo].[furniture] ([furnitureID], [categoryID], [styleID], [description], [dailyRentalRate], [fineRate]) VALUES (2, 1, 2, N'King', CAST(25.00 AS Decimal(6, 2)), CAST(27.50 AS Decimal(6, 2)))
INSERT [dbo].[furniture] ([furnitureID], [categoryID], [styleID], [description], [dailyRentalRate], [fineRate]) VALUES (3, 2, 1, N'Full', CAST(50.00 AS Decimal(6, 2)), CAST(55.00 AS Decimal(6, 2)))
INSERT [dbo].[furniture] ([furnitureID], [categoryID], [styleID], [description], [dailyRentalRate], [fineRate]) VALUES (4, 2, 2, N'Loveseat', CAST(45.00 AS Decimal(6, 2)), CAST(49.50 AS Decimal(6, 2)))
INSERT [dbo].[furniture] ([furnitureID], [categoryID], [styleID], [description], [dailyRentalRate], [fineRate]) VALUES (5, 3, 1, N'4-Piece', CAST(75.00 AS Decimal(6, 2)), CAST(82.50 AS Decimal(6, 2)))
INSERT [dbo].[furniture] ([furnitureID], [categoryID], [styleID], [description], [dailyRentalRate], [fineRate]) VALUES (6, 3, 2, N'5-Piece', CAST(80.00 AS Decimal(6, 2)), CAST(88.00 AS Decimal(6, 2)))
INSERT [dbo].[furniture] ([furnitureID], [categoryID], [styleID], [description], [dailyRentalRate], [fineRate]) VALUES (7, 4, 1, N'Standard', CAST(20.00 AS Decimal(6, 2)), CAST(22.00 AS Decimal(6, 2)))
INSERT [dbo].[furniture] ([furnitureID], [categoryID], [styleID], [description], [dailyRentalRate], [fineRate]) VALUES (8, 4, 2, N'Recliner', CAST(30.00 AS Decimal(6, 2)), CAST(33.00 AS Decimal(6, 2)))
SET IDENTITY_INSERT [dbo].[furniture] OFF
INSERT [dbo].[furnitureItem] ([serialNumber], [furnitureID]) VALUES (101, 1)
INSERT [dbo].[furnitureItem] ([serialNumber], [furnitureID]) VALUES (102, 1)
INSERT [dbo].[furnitureItem] ([serialNumber], [furnitureID]) VALUES (103, 1)
INSERT [dbo].[furnitureItem] ([serialNumber], [furnitureID]) VALUES (104, 1)
INSERT [dbo].[furnitureItem] ([serialNumber], [furnitureID]) VALUES (105, 2)
INSERT [dbo].[furnitureItem] ([serialNumber], [furnitureID]) VALUES (106, 2)
INSERT [dbo].[furnitureItem] ([serialNumber], [furnitureID]) VALUES (107, 2)
INSERT [dbo].[furnitureItem] ([serialNumber], [furnitureID]) VALUES (108, 2)
INSERT [dbo].[furnitureItem] ([serialNumber], [furnitureID]) VALUES (109, 3)
INSERT [dbo].[furnitureItem] ([serialNumber], [furnitureID]) VALUES (110, 3)
INSERT [dbo].[furnitureItem] ([serialNumber], [furnitureID]) VALUES (111, 3)
INSERT [dbo].[furnitureItem] ([serialNumber], [furnitureID]) VALUES (112, 3)
INSERT [dbo].[furnitureItem] ([serialNumber], [furnitureID]) VALUES (113, 4)
INSERT [dbo].[furnitureItem] ([serialNumber], [furnitureID]) VALUES (114, 4)
INSERT [dbo].[furnitureItem] ([serialNumber], [furnitureID]) VALUES (115, 4)
INSERT [dbo].[furnitureItem] ([serialNumber], [furnitureID]) VALUES (116, 4)
INSERT [dbo].[furnitureItem] ([serialNumber], [furnitureID]) VALUES (117, 5)
INSERT [dbo].[furnitureItem] ([serialNumber], [furnitureID]) VALUES (118, 5)
INSERT [dbo].[furnitureItem] ([serialNumber], [furnitureID]) VALUES (119, 5)
INSERT [dbo].[furnitureItem] ([serialNumber], [furnitureID]) VALUES (120, 5)
INSERT [dbo].[furnitureItem] ([serialNumber], [furnitureID]) VALUES (121, 6)
INSERT [dbo].[furnitureItem] ([serialNumber], [furnitureID]) VALUES (122, 6)
INSERT [dbo].[furnitureItem] ([serialNumber], [furnitureID]) VALUES (123, 6)
INSERT [dbo].[furnitureItem] ([serialNumber], [furnitureID]) VALUES (124, 6)
INSERT [dbo].[furnitureItem] ([serialNumber], [furnitureID]) VALUES (125, 7)
INSERT [dbo].[furnitureItem] ([serialNumber], [furnitureID]) VALUES (126, 7)
INSERT [dbo].[furnitureItem] ([serialNumber], [furnitureID]) VALUES (127, 7)
INSERT [dbo].[furnitureItem] ([serialNumber], [furnitureID]) VALUES (128, 7)
INSERT [dbo].[furnitureItem] ([serialNumber], [furnitureID]) VALUES (129, 8)
INSERT [dbo].[furnitureItem] ([serialNumber], [furnitureID]) VALUES (130, 8)
INSERT [dbo].[furnitureItem] ([serialNumber], [furnitureID]) VALUES (131, 8)
INSERT [dbo].[furnitureItem] ([serialNumber], [furnitureID]) VALUES (132, 8)
SET IDENTITY_INSERT [dbo].[member] ON 

INSERT [dbo].[member] ([memberID], [DOB], [fName], [minit], [lName], [phone], [email], [address1], [address2], [city], [state], [zip]) VALUES (1, CAST(N'1979-07-13' AS Date), N'Laurel', N'A', N'Jones', N'4445555550', N'ljones@email.com', N'1 Main St', NULL, N'Atlanta', N'GA', N'30303')
INSERT [dbo].[member] ([memberID], [DOB], [fName], [minit], [lName], [phone], [email], [address1], [address2], [city], [state], [zip]) VALUES (2, CAST(N'1986-10-13' AS Date), N'Cameron', N'V', N'Pew', N'4445555551', N'cpew@email.com', N'2 Main St', NULL, N'Atlanta', N'GA', N'30303')
INSERT [dbo].[member] ([memberID], [DOB], [fName], [minit], [lName], [phone], [email], [address1], [address2], [city], [state], [zip]) VALUES (3, CAST(N'1989-05-01' AS Date), N'Stewart', N'G', N'Grubbs', N'4445555553', N'sgrubbs@email.com', N'4 Peachtree St', NULL, N'Atlanta', N'GA', N'30303')
INSERT [dbo].[member] ([memberID], [DOB], [fName], [minit], [lName], [phone], [email], [address1], [address2], [city], [state], [zip]) VALUES (4, CAST(N'1995-04-13' AS Date), N'Rebecca', N'H', N'Hardy', N'4445555554', N'rhardy@email.com', N'5 Peachtree St', NULL, N'Atlanta', N'GA', N'30303')
INSERT [dbo].[member] ([memberID], [DOB], [fName], [minit], [lName], [phone], [email], [address1], [address2], [city], [state], [zip]) VALUES (5, CAST(N'1993-03-04' AS Date), N'Janet', N'S', N'Hamilton', N'4445555555', N'jhamilton@email.com', N'6 Peachtree St', NULL, N'Atlanta', N'GA', N'30303')
INSERT [dbo].[member] ([memberID], [DOB], [fName], [minit], [lName], [phone], [email], [address1], [address2], [city], [state], [zip]) VALUES (6, CAST(N'1969-07-20' AS Date), N'Jeff', N'B', N'Harris', N'4445555556', N'jharris@email.com', N'7 Peachtree St', NULL, N'Atlanta', N'GA', N'30303')
INSERT [dbo].[member] ([memberID], [DOB], [fName], [minit], [lName], [phone], [email], [address1], [address2], [city], [state], [zip]) VALUES (7, CAST(N'1976-02-04' AS Date), N'Walter', N'F', N'Max', N'4445555557', N'wmax@email.com', N'8 Peachtree St', NULL, N'Atlanta', N'GA', N'30303')
SET IDENTITY_INSERT [dbo].[member] OFF
SET IDENTITY_INSERT [dbo].[rental] ON 

INSERT [dbo].[rental] ([rentalID], [memberID], [empID], [rentalDate]) VALUES (1, 3, 1, CAST(N'2015-03-01T00:00:00.000' AS DateTime))
INSERT [dbo].[rental] ([rentalID], [memberID], [empID], [rentalDate]) VALUES (2, 7, 3, CAST(N'2015-05-06T00:00:00.000' AS DateTime))
INSERT [dbo].[rental] ([rentalID], [memberID], [empID], [rentalDate]) VALUES (3, 1, 2, CAST(N'2015-10-13T00:00:00.000' AS DateTime))
INSERT [dbo].[rental] ([rentalID], [memberID], [empID], [rentalDate]) VALUES (4, 2, 1, CAST(N'2015-11-14T00:00:00.000' AS DateTime))
INSERT [dbo].[rental] ([rentalID], [memberID], [empID], [rentalDate]) VALUES (5, 4, 3, CAST(N'2015-02-04T00:00:00.000' AS DateTime))
INSERT [dbo].[rental] ([rentalID], [memberID], [empID], [rentalDate]) VALUES (6, 5, 2, CAST(N'2015-03-02T00:00:00.000' AS DateTime))
INSERT [dbo].[rental] ([rentalID], [memberID], [empID], [rentalDate]) VALUES (7, 6, 1, CAST(N'2015-03-08T00:00:00.000' AS DateTime))
INSERT [dbo].[rental] ([rentalID], [memberID], [empID], [rentalDate]) VALUES (8, 7, 3, CAST(N'2014-01-07T00:00:00.000' AS DateTime))
INSERT [dbo].[rental] ([rentalID], [memberID], [empID], [rentalDate]) VALUES (9, 1, 2, CAST(N'2016-03-09T00:00:00.000' AS DateTime))
INSERT [dbo].[rental] ([rentalID], [memberID], [empID], [rentalDate]) VALUES (10, 2, 1, CAST(N'2017-03-23T00:00:00.000' AS DateTime))
INSERT [dbo].[rental] ([rentalID], [memberID], [empID], [rentalDate]) VALUES (11, 3, 1, CAST(N'2017-03-17T00:00:00.000' AS DateTime))
INSERT [dbo].[rental] ([rentalID], [memberID], [empID], [rentalDate]) VALUES (12, 4, 3, CAST(N'2017-09-12T00:00:00.000' AS DateTime))
INSERT [dbo].[rental] ([rentalID], [memberID], [empID], [rentalDate]) VALUES (13, 5, 2, CAST(N'2017-01-02T00:00:00.000' AS DateTime))
INSERT [dbo].[rental] ([rentalID], [memberID], [empID], [rentalDate]) VALUES (14, 6, 1, CAST(N'2014-03-19T00:00:00.000' AS DateTime))
INSERT [dbo].[rental] ([rentalID], [memberID], [empID], [rentalDate]) VALUES (15, 7, 2, CAST(N'2016-12-02T00:00:00.000' AS DateTime))
INSERT [dbo].[rental] ([rentalID], [memberID], [empID], [rentalDate]) VALUES (16, 6, 3, CAST(N'2016-09-06T00:00:00.000' AS DateTime))
INSERT [dbo].[rental] ([rentalID], [memberID], [empID], [rentalDate]) VALUES (17, 1, 2, CAST(N'2016-03-22T00:00:00.000' AS DateTime))
INSERT [dbo].[rental] ([rentalID], [memberID], [empID], [rentalDate]) VALUES (18, 2, 1, CAST(N'2014-03-30T00:00:00.000' AS DateTime))
INSERT [dbo].[rental] ([rentalID], [memberID], [empID], [rentalDate]) VALUES (19, 3, 1, CAST(N'2015-03-11T00:00:00.000' AS DateTime))
INSERT [dbo].[rental] ([rentalID], [memberID], [empID], [rentalDate]) VALUES (20, 4, 3, CAST(N'2017-03-19T00:00:00.000' AS DateTime))
INSERT [dbo].[rental] ([rentalID], [memberID], [empID], [rentalDate]) VALUES (21, 5, 2, CAST(N'2015-05-02T00:00:00.000' AS DateTime))
INSERT [dbo].[rental] ([rentalID], [memberID], [empID], [rentalDate]) VALUES (22, 6, 1, CAST(N'2014-08-15T00:00:00.000' AS DateTime))
INSERT [dbo].[rental] ([rentalID], [memberID], [empID], [rentalDate]) VALUES (23, 7, 3, CAST(N'2014-06-02T00:00:00.000' AS DateTime))
INSERT [dbo].[rental] ([rentalID], [memberID], [empID], [rentalDate]) VALUES (24, 2, 3, CAST(N'2014-03-18T00:00:00.000' AS DateTime))
SET IDENTITY_INSERT [dbo].[rental] OFF
INSERT [dbo].[rentalLineItem] ([rentalID], [serialNumber], [returnID], [dueDate]) VALUES (1, 101, 1, CAST(N'2015-04-01T00:00:00.000' AS DateTime))
INSERT [dbo].[rentalLineItem] ([rentalID], [serialNumber], [returnID], [dueDate]) VALUES (2, 105, 2, CAST(N'2015-06-06T00:00:00.000' AS DateTime))
INSERT [dbo].[rentalLineItem] ([rentalID], [serialNumber], [returnID], [dueDate]) VALUES (3, 109, 3, CAST(N'2015-11-13T00:00:00.000' AS DateTime))
INSERT [dbo].[rentalLineItem] ([rentalID], [serialNumber], [returnID], [dueDate]) VALUES (4, 113, 4, CAST(N'2015-12-14T00:00:00.000' AS DateTime))
INSERT [dbo].[rentalLineItem] ([rentalID], [serialNumber], [returnID], [dueDate]) VALUES (5, 117, 5, CAST(N'2015-03-04T00:00:00.000' AS DateTime))
INSERT [dbo].[rentalLineItem] ([rentalID], [serialNumber], [returnID], [dueDate]) VALUES (6, 121, 6, CAST(N'2015-04-02T00:00:00.000' AS DateTime))
INSERT [dbo].[rentalLineItem] ([rentalID], [serialNumber], [returnID], [dueDate]) VALUES (7, 125, 7, CAST(N'2015-04-08T00:00:00.000' AS DateTime))
INSERT [dbo].[rentalLineItem] ([rentalID], [serialNumber], [returnID], [dueDate]) VALUES (8, 129, 8, CAST(N'2014-02-07T00:00:00.000' AS DateTime))
INSERT [dbo].[rentalLineItem] ([rentalID], [serialNumber], [returnID], [dueDate]) VALUES (9, 102, 9, CAST(N'2016-04-09T00:00:00.000' AS DateTime))
INSERT [dbo].[rentalLineItem] ([rentalID], [serialNumber], [returnID], [dueDate]) VALUES (10, 106, 10, CAST(N'2017-04-23T00:00:00.000' AS DateTime))
INSERT [dbo].[rentalLineItem] ([rentalID], [serialNumber], [returnID], [dueDate]) VALUES (11, 110, 11, CAST(N'2017-04-17T00:00:00.000' AS DateTime))
INSERT [dbo].[rentalLineItem] ([rentalID], [serialNumber], [returnID], [dueDate]) VALUES (12, 114, 12, CAST(N'2017-10-12T00:00:00.000' AS DateTime))
INSERT [dbo].[rentalLineItem] ([rentalID], [serialNumber], [returnID], [dueDate]) VALUES (13, 118, 13, CAST(N'2017-02-02T00:00:00.000' AS DateTime))
INSERT [dbo].[rentalLineItem] ([rentalID], [serialNumber], [returnID], [dueDate]) VALUES (14, 122, 14, CAST(N'2014-04-19T00:00:00.000' AS DateTime))
INSERT [dbo].[rentalLineItem] ([rentalID], [serialNumber], [returnID], [dueDate]) VALUES (15, 126, 15, CAST(N'2016-12-23T00:00:00.000' AS DateTime))
INSERT [dbo].[rentalLineItem] ([rentalID], [serialNumber], [returnID], [dueDate]) VALUES (16, 130, 16, CAST(N'2016-10-06T00:00:00.000' AS DateTime))
INSERT [dbo].[rentalLineItem] ([rentalID], [serialNumber], [returnID], [dueDate]) VALUES (17, 103, 17, CAST(N'2016-04-22T00:00:00.000' AS DateTime))
INSERT [dbo].[rentalLineItem] ([rentalID], [serialNumber], [returnID], [dueDate]) VALUES (18, 107, 18, CAST(N'2014-04-30T00:00:00.000' AS DateTime))
INSERT [dbo].[rentalLineItem] ([rentalID], [serialNumber], [returnID], [dueDate]) VALUES (19, 111, 19, CAST(N'2015-04-11T00:00:00.000' AS DateTime))
INSERT [dbo].[rentalLineItem] ([rentalID], [serialNumber], [returnID], [dueDate]) VALUES (20, 115, 20, CAST(N'2017-04-19T00:00:00.000' AS DateTime))
INSERT [dbo].[rentalLineItem] ([rentalID], [serialNumber], [returnID], [dueDate]) VALUES (21, 119, 21, CAST(N'2015-06-02T00:00:00.000' AS DateTime))
INSERT [dbo].[rentalLineItem] ([rentalID], [serialNumber], [returnID], [dueDate]) VALUES (22, 123, 22, CAST(N'2014-09-15T00:00:00.000' AS DateTime))
INSERT [dbo].[rentalLineItem] ([rentalID], [serialNumber], [returnID], [dueDate]) VALUES (23, 127, 23, CAST(N'2014-07-02T00:00:00.000' AS DateTime))
INSERT [dbo].[rentalLineItem] ([rentalID], [serialNumber], [returnID], [dueDate]) VALUES (24, 131, 24, CAST(N'2014-04-18T00:00:00.000' AS DateTime))
SET IDENTITY_INSERT [dbo].[return] ON 

INSERT [dbo].[return] ([returnID], [empID], [dateReturned]) VALUES (1, 2, CAST(N'2015-04-01' AS Date))
INSERT [dbo].[return] ([returnID], [empID], [dateReturned]) VALUES (2, 1, CAST(N'2015-06-07' AS Date))
INSERT [dbo].[return] ([returnID], [empID], [dateReturned]) VALUES (3, 3, CAST(N'2015-11-13' AS Date))
INSERT [dbo].[return] ([returnID], [empID], [dateReturned]) VALUES (4, 2, CAST(N'2015-12-14' AS Date))
INSERT [dbo].[return] ([returnID], [empID], [dateReturned]) VALUES (5, 1, CAST(N'2015-03-06' AS Date))
INSERT [dbo].[return] ([returnID], [empID], [dateReturned]) VALUES (6, 3, CAST(N'2015-04-02' AS Date))
INSERT [dbo].[return] ([returnID], [empID], [dateReturned]) VALUES (7, 2, CAST(N'2015-04-08' AS Date))
INSERT [dbo].[return] ([returnID], [empID], [dateReturned]) VALUES (8, 1, CAST(N'2014-02-07' AS Date))
INSERT [dbo].[return] ([returnID], [empID], [dateReturned]) VALUES (9, 3, CAST(N'2016-04-10' AS Date))
INSERT [dbo].[return] ([returnID], [empID], [dateReturned]) VALUES (10, 2, CAST(N'2017-04-23' AS Date))
INSERT [dbo].[return] ([returnID], [empID], [dateReturned]) VALUES (11, 2, CAST(N'2017-04-17' AS Date))
INSERT [dbo].[return] ([returnID], [empID], [dateReturned]) VALUES (12, 1, CAST(N'2017-10-12' AS Date))
INSERT [dbo].[return] ([returnID], [empID], [dateReturned]) VALUES (13, 3, CAST(N'2017-02-02' AS Date))
INSERT [dbo].[return] ([returnID], [empID], [dateReturned]) VALUES (14, 2, CAST(N'2014-04-19' AS Date))
INSERT [dbo].[return] ([returnID], [empID], [dateReturned]) VALUES (15, 3, CAST(N'2016-12-23' AS Date))
INSERT [dbo].[return] ([returnID], [empID], [dateReturned]) VALUES (16, 1, CAST(N'2016-10-06' AS Date))
INSERT [dbo].[return] ([returnID], [empID], [dateReturned]) VALUES (17, 2, CAST(N'2016-04-22' AS Date))
INSERT [dbo].[return] ([returnID], [empID], [dateReturned]) VALUES (18, 2, CAST(N'2014-04-30' AS Date))
INSERT [dbo].[return] ([returnID], [empID], [dateReturned]) VALUES (19, 2, CAST(N'2015-04-11' AS Date))
INSERT [dbo].[return] ([returnID], [empID], [dateReturned]) VALUES (20, 1, CAST(N'2017-04-19' AS Date))
INSERT [dbo].[return] ([returnID], [empID], [dateReturned]) VALUES (21, 3, CAST(N'2015-06-05' AS Date))
INSERT [dbo].[return] ([returnID], [empID], [dateReturned]) VALUES (22, 2, CAST(N'2014-09-16' AS Date))
INSERT [dbo].[return] ([returnID], [empID], [dateReturned]) VALUES (23, 1, CAST(N'2014-07-02' AS Date))
INSERT [dbo].[return] ([returnID], [empID], [dateReturned]) VALUES (24, 1, CAST(N'2014-04-18' AS Date))
SET IDENTITY_INSERT [dbo].[return] OFF
SET IDENTITY_INSERT [dbo].[style] ON 

INSERT [dbo].[style] ([styleID], [styleName]) VALUES (1, N'Modern')
INSERT [dbo].[style] ([styleID], [styleName]) VALUES (2, N'Classic')
SET IDENTITY_INSERT [dbo].[style] OFF
ALTER TABLE [dbo].[cart]  WITH CHECK ADD  CONSTRAINT [FK_cart_employee] FOREIGN KEY([empID])
REFERENCES [dbo].[employee] ([empID])
GO
ALTER TABLE [dbo].[cart] CHECK CONSTRAINT [FK_cart_employee]
GO
ALTER TABLE [dbo].[cart]  WITH CHECK ADD  CONSTRAINT [FK_cart_furnitureItem] FOREIGN KEY([serialNumber])
REFERENCES [dbo].[furnitureItem] ([serialNumber])
GO
ALTER TABLE [dbo].[cart] CHECK CONSTRAINT [FK_cart_furnitureItem]
GO
ALTER TABLE [dbo].[cart]  WITH CHECK ADD  CONSTRAINT [FK_cart_member] FOREIGN KEY([memberID])
REFERENCES [dbo].[member] ([memberID])
GO
ALTER TABLE [dbo].[cart] CHECK CONSTRAINT [FK_cart_member]
GO
ALTER TABLE [dbo].[empSystem]  WITH CHECK ADD  CONSTRAINT [FK_empSystem_employee] FOREIGN KEY([empID])
REFERENCES [dbo].[employee] ([empID])
GO
ALTER TABLE [dbo].[empSystem] CHECK CONSTRAINT [FK_empSystem_employee]
GO
ALTER TABLE [dbo].[furniture]  WITH CHECK ADD  CONSTRAINT [FK_furniture_category] FOREIGN KEY([categoryID])
REFERENCES [dbo].[category] ([categoryID])
GO
ALTER TABLE [dbo].[furniture] CHECK CONSTRAINT [FK_furniture_category]
GO
ALTER TABLE [dbo].[furniture]  WITH CHECK ADD  CONSTRAINT [FK_furniture_style] FOREIGN KEY([styleID])
REFERENCES [dbo].[style] ([styleID])
ON UPDATE CASCADE
GO
ALTER TABLE [dbo].[furniture] CHECK CONSTRAINT [FK_furniture_style]
GO
ALTER TABLE [dbo].[furnitureItem]  WITH CHECK ADD  CONSTRAINT [FK_furnitureItem_furniture] FOREIGN KEY([furnitureID])
REFERENCES [dbo].[furniture] ([furnitureID])
GO
ALTER TABLE [dbo].[furnitureItem] CHECK CONSTRAINT [FK_furnitureItem_furniture]
GO
ALTER TABLE [dbo].[rental]  WITH CHECK ADD  CONSTRAINT [FK_rental_employee] FOREIGN KEY([empID])
REFERENCES [dbo].[employee] ([empID])
GO
ALTER TABLE [dbo].[rental] CHECK CONSTRAINT [FK_rental_employee]
GO
ALTER TABLE [dbo].[rental]  WITH CHECK ADD  CONSTRAINT [FK_rental_member] FOREIGN KEY([memberID])
REFERENCES [dbo].[member] ([memberID])
GO
ALTER TABLE [dbo].[rental] CHECK CONSTRAINT [FK_rental_member]
GO
ALTER TABLE [dbo].[rentalLineItem]  WITH CHECK ADD  CONSTRAINT [FK_rentalLineItem_furnitureItem] FOREIGN KEY([serialNumber])
REFERENCES [dbo].[furnitureItem] ([serialNumber])
GO
ALTER TABLE [dbo].[rentalLineItem] CHECK CONSTRAINT [FK_rentalLineItem_furnitureItem]
GO
ALTER TABLE [dbo].[rentalLineItem]  WITH CHECK ADD  CONSTRAINT [FK_rentalLineItem_rental] FOREIGN KEY([rentalID])
REFERENCES [dbo].[rental] ([rentalID])
GO
ALTER TABLE [dbo].[rentalLineItem] CHECK CONSTRAINT [FK_rentalLineItem_rental]
GO
ALTER TABLE [dbo].[rentalLineItem]  WITH CHECK ADD  CONSTRAINT [FK_rentalLineItem_return] FOREIGN KEY([returnID])
REFERENCES [dbo].[return] ([returnID])
GO
ALTER TABLE [dbo].[rentalLineItem] CHECK CONSTRAINT [FK_rentalLineItem_return]
GO
ALTER TABLE [dbo].[return]  WITH CHECK ADD  CONSTRAINT [FK_return_employee] FOREIGN KEY([empID])
REFERENCES [dbo].[employee] ([empID])
GO
ALTER TABLE [dbo].[return] CHECK CONSTRAINT [FK_return_employee]
GO
USE [master]
GO
ALTER DATABASE [CS6232-G3] SET  READ_WRITE 
GO
